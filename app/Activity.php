<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Activity extends Model
{
    protected $fillable = [
        'title',
        'content',
        'slug',
        'visit',
        'images',
        'videos',
        'programmable_id',
        'programmable_Type',
    ];
    protected $casts = [
        'videos'=>'array',
        'images'=>'array',
    ];
    use HasSlug,SoftDeletes;
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['title', 'created_at'])
            ->saveSlugsTo('slug')
            ->usingSeparator('_')
            ->usingLanguage('ar');
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }




    //relationships
    public function programmable()
    {
        return $this->morphTo();
    }
}
