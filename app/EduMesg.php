<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EduMesg extends Model
{
    protected $fillable = [
        'text',
        'file',

    ];

    use SoftDeletes;
}
