<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Program;
use Illuminate\Http\Request;

class ActivityController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Activity::orderByDesc('id')->paginate('12');
        return view('backend.pages.activities.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.activities.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title'=>['required','max:250','string'],
            'content'=>['required','string'],
            'program'=>['required'],
            'file' => 'nullable',
            'file.*' => 'mimes:jpeg,jpg,png',
            'youtubeLink' => 'nullable',
        ]);
        if($data['program'] == '-1'){
            $data['program'] = null;
            $data['program_type'] = null;
        }else{
            $data['program_type'] = Program::class;
        }
        if (isset($data['youtubeLink']) && $data['youtubeLink'][0] != null){
            foreach ($data['youtubeLink'] as $key=>$link){
                if (preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#",$link,$matches)) {
                    preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#", $link, $matches);
                    if (isset($matches[0][0])) {
                        $data['youtubeLink'][$key] = $matches[0][0];
                    }
                }else{
                    session()->flash('error','عذراً، من فضلك قم بالتأكد من جميع روابط اليوتيوب التي أدخلتها..');
                    return back();
                }
            }
        }

        $images = $this->uploadMultiImages($request, 'file','activities' );
         //dd(array_values($data['youtubeLink']));
        $activity = New Activity();
        $activity->title = $data['title'];
        $activity->content = $data['content'];
        $activity->images = $images;
        $activity->programmable_id = $data['program'];
        $activity->programmable_type = $data['program_type'];
        $activity->videos = array_values($data['youtubeLink']);
        $activity->save();
        session()->flash('success','تم حفظ العنصر بنجاح..');
        return redirect()->route('activities.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function show(Activity $activity)
    {
        return view('backend.pages.activities.show',compact('activity'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function edit(Activity $activity)
    {
        return view('backend.pages.activities.edit',compact('activity'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Activity $activity)
    {

        $data = $request->validate([
            'title'=>['required','max:250','string'],
            'content'=>['required','string'],
            'program' => 'required',
            'file' => 'nullable',
            'file.*' => 'mimes:jpeg,jpg,png',
            'youtubeLink' => 'nullable',
        ]);
        if($data['program'] == '-1'){
            $data['program'] = null;
            $data['program_type'] = null;
        }else{
            $data['program_type'] = Program::class;
        }
        $activity ->title = $data ['title'];
        $activity ->content = $data ['content'];
        $activity ->programmable_id = $data ['program'];
        $activity ->programmable_type = $data ['program_type'];
        if($request->hasFile('file')){
            $activity ->images = $this->uploadMultiImages($request, 'file','activities' );
        }


        if (isset($data['youtubeLink']) && !is_null($data['youtubeLink'][0]) ){
            foreach ($data['youtubeLink'] as $key=>$link){
                if (preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#",$link,$matches)) {
                    preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#", $link, $matches);
                    if (isset($matches[0][0])) {
                        $data['youtubeLink'][$key] = $matches[0][0];
                    }
                }else{
                    session()->flash('error','عذراً، من فضلك قم بالتأكد من جميع روابط اليوتيوب التي أدخلتها..');
                    return back();
                }
            }
            $activity->videos = $data['youtubeLink'];
        }
        $activity->save();
        session()->flash('success','تم تحديث بيانات العنصر بنجاح..');
        return redirect()->route('activities.show',$activity);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Activity  $activity
     * @return \Illuminate\Http\Response
     */
    public function destroy(Activity $activity)
    {
       if( !is_null($activity->images)){
           foreach ($activity->images as $key => $image){
               \File::Delete($image);
           }
       }
       $activity->delete();
       session()->flash('success','تم حذف العنصر بنجاح..');
       return redirect()->route('activities.index');
    }
}
