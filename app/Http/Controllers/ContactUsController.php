<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\Mail\contact_us;
use App\Http\Requests\postContactRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactUsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['store']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = ContactUs::orderByDesc('id')->paginate();
        return view('backend.emails.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(postContactRequest $request)
    {
        $data=$request->validated();


        $email =new ContactUs();
        $email -> name = $data['name'];
        $email -> phone = $data['phone'];
        $email ->for = $data['reason'];
        $email -> message = $data['message'];
        $email->save();
        session()->flash('sent','تم إرسال رسالتك بنجاح، شكراً لك..');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function show(ContactUs $contactUs)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function edit(ContactUs $contactUs)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $mail)
    {
        $email = ContactUs::find($mail);
        $email->seen = 1;
        $email->save();
        session()->flash('success','تم تحديث حالة العنصر بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ContactUs  $contactUs
     * @return \Illuminate\Http\Response
     */
    public function destroy(ContactUs $contactUs)
    {
        //
    }
}
