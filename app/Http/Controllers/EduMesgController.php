<?php

namespace App\Http\Controllers;

use App\EduMesg;
use Illuminate\Http\Request;

class EduMesgController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = EduMesg::orderByDesc('id')->get();
        return view('backend.pages.edu.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.edu.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'text'=>'required',
        ]);
        $item = New EduMesg();
        $item->text = $data ['text'];
        $item->save();
        session()->flash('success','تم حفظ العنصر بنجاح..');
        return redirect()->route('edumesgs.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EduMesg  $eduMesg
     * @return \Illuminate\Http\Response
     */
    public function show(EduMesg $eduMesg)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EduMesg  $eduMesg
     * @return \Illuminate\Http\Response
     */
    public function edit(EduMesg $eduMesg)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EduMesg  $eduMesg
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $eduMesg)
    {
        $item = EduMesg::findOrFail($eduMesg);
        $data = $request->validate([
            'text'=>'required',
        ]);
        $item->text = $data['text'];
        $item->save();
        session()->flash('success','تم تحديث بيانات العنصر بنجاح..');

        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EduMesg  $eduMesg
     * @return \Illuminate\Http\Response
     */
    public function destroy($eduMesg)
    {
        $item = EduMesg::findOrFail($eduMesg);
        $item->delete();
        session()->flash('success','تم حذف العنصر بنجاح..');
        return back();
    }
}
