<?php

namespace App\Http\Controllers;

use App\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Faq::orderByDesc('id')->paginate(20);
        return view('backend.pages.faqs.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.faqs.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
           'title'=>'required|string',
           'content'=>'required|string',
        ]);
        $faq = New Faq();
        $faq -> title = $data['title'];
        $faq -> content = $data['content'];
        $faq->save();
        session()->flash('success','تم حفظ العنصر بنجاح..');
        return redirect()->route('faqs.show',$faq);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        $item = $faq;
        return view('backend.pages.faqs.show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {
        $item = $faq;
        return view('backend.pages.faqs.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
         $data = $request->validate([
        'title'=>'required|string',
        'content'=>'required|string',
        ]);

        $faq -> title = $data['title'];
        $faq -> content = $data['content'];
        $faq->save();
        session()->flash('success','تم تحديث بيانات العنصر بنجاح..');
        return redirect()->route('faqs.show',$faq);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
        $faq->delete();
        session()->flash('success','تمت عملية الحذف بنجاح');
        return redirect()->route('faqs.index');
    }
}
