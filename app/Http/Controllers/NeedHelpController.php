<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreHelpRequiest;
use App\NeedHelp;
use App\Program;
use Illuminate\Http\Request;

class NeedHelpController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(StoreHelpRequiest $request,  $program)
    {
        $data = $request->validated();
        $program = Program::where('slug',$program)->first();
        if(is_null($data['text']) && is_null($data['file'])){
            session()->flash('error','قم بإدخال نص أو صورة..');
            return back();
        }
        $image = $this->uploadImage($request,'file','helps');
        $program->helps()->create([
            'text'=>$data['text'],
            'file'=>$image,
        ]);
        session()->flash('success','تم حفظ العنصر بنجاح..');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\NeedHelp  $needHelp
     * @return \Illuminate\Http\Response
     */
    public function show(NeedHelp $needHelp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\NeedHelp  $needHelp
     * @return \Illuminate\Http\Response
     */
    public function edit(NeedHelp $needHelp)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\NeedHelp  $needHelp
     * @return \Illuminate\Http\Response
     */
    public function update(StoreHelpRequiest $request,  $needHelp)
    {
        $data = $request->validated();
        $needHelp = NeedHelp::where('id',$needHelp)->first();
        if(is_null($data['text']) && is_null($data['file'])){
            session()->flash('error','قم بإدخال نص أو صورة..');
            return back();
        }
        if($request->hasFile('file')){
            if(!is_null($needHelp->file)){
            \File::delete($needHelp->file);
            }
        }
        $image = $this->uploadImage($request,'file','helps');

        $needHelp->text = $data['text'];
        $needHelp->file = $image;
        $needHelp->save();
        session()->flash('success','تم تحديث بيانات العنصر بنجاح..');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\NeedHelp  $needHelp
     * @return \Illuminate\Http\Response
     */
    public function destroy( $needHelp)
    {
        $needHelp = NeedHelp::where('id',$needHelp)->first();
        if(!is_null($needHelp->file)){
            \File::delete($needHelp->file);
        }
        $needHelp->delete();
        session()->flash('success','تم  حذف العنصر بنجاح..');
        return back();

    }
}
