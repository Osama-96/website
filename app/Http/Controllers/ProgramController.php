<?php

namespace App\Http\Controllers;

use App\Program;
use Illuminate\Http\Request;

class ProgramController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Program::paginate();
        return view('backend.pages.programs.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('backend.pages.programs.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title'=>['string','required'],
            'content'=>['string','required'],
            'story_sub_title'=>['string','required'],
            'story_text'=>['string','required'],
            'can_help_sub_title'=>['string','required'],
        ]);
        $item = New Program();
        $item ->title =                 $data['title'];
        $item ->content =               $data['content'];
        $item ->story_sub_title =       $data['story_sub_title'];
        $item ->story_text =            $data['story_text'];
        $item ->can_help_sub_title =    $data['can_help_sub_title'];
        $item->save();
        session()->flash('success','تم حفظ العنصر بنجاح..');
        return redirect()->route('programs.show',$item);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function show(Program $program)
    {
        $item = $program;
        return view('backend.pages.programs.show',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function edit(Program $program)
    {
        $item = $program;
        return view('backend.pages.programs.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Program $program)
    {
        $data = $request->validate([
            'title'=>['string','required'],
            'content'=>['string','required'],
            'story_sub_title'=>['string','required'],
            'story_text'=>['string','required'],
            'can_help_sub_title'=>['string','required'],
        ]);
        $item = $program;
        $item ->title =                 $data['title'];
        $item ->content =               $data['content'];
        $item ->story_sub_title =       $data['story_sub_title'];
        $item ->story_text =            $data['story_text'];
        $item ->can_help_sub_title =    $data['can_help_sub_title'];
        $item->save();
        session()->flash('success','تم تحديث بيانات العنصر بنجاح..');
        return redirect()->route('programs.show',$item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Program  $program
     * @return \Illuminate\Http\Response
     */
    public function destroy(Program $program)
    {
        if($program->stories()->count() >0){
            foreach ($program->stories as $key => $story){
                $story->delete();
            }
        }
        if($program->helps()->count() >0){
            foreach ($program->helps as $key => $help){
                if(!is_null($help->file)){
                    \File::delete($help->file);
                }
                $help->delete();
            }
        }
        if($program->activities()->count() >0){
            foreach ($program->activities as $key => $activity){
                $activity->programmable_id = null;
                $activity->programmable_type = null;
                $activity->save();
            }
        }
        $program->delete();
        session()->flash('success','تم حذف العنصر بنجاح..');
        return redirect()->route('programs.index');
    }
}
