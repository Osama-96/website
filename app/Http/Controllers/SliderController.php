<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostSlideRequest;
use App\Http\Requests\UpdateSlideRequest;
use App\Slider;
use App\Traits\uploadImage;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Slider::all();
        return view('backend.pages.slider.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.slider.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostSlideRequest $request)
    {
        $data = $request->validated();
        if(Slider::count() >= 5){
            return back()->with('error','عفوا لا يمكنك حفظ أكثر من 5 Slides وذلك تجنباً لبطء فتح الموقع');
        }
        $image = $this->uploadImage($request,'file','slides');
        $item = New Slider();
        $item->text = $data['text'];
        $item->file = $image;
        $item->link = $data['link'];
        $item->save();
        session()->flash('success','تم حفظ العنصر بنجاح');
        return redirect()->route('slides.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSlideRequest $request, Slider  $slide)
    {

        $data = $request->validated();
        $item = $slide;

        if($request->hasFile('file')){
            \File::Delete($slide->file);
            $image= $this->uploadImage($request,'file','slides');
            $item->file = $image;
        }
        $item->text = $data['text'];
        $item->link = $data['link'];
        $item->save();
        session()->flash('success','تم تحديث بيانات العنصر بنجاح');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
        $slider = Slider::findOrFail($id);
        \File::Delete($slider->file);
        $slider->delete();
        session()->flash('success','تم حذف العنصر بنجاح');
        return back();
    }
}
