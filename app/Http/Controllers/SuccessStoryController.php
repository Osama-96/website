<?php

namespace App\Http\Controllers;

use App\Program;
use App\SuccessStory;
use Illuminate\Http\Request;

class SuccessStoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $slug)
    {
        $program = Program::where('slug',$slug)->first();
        $data = $request->validate([
            'title'=>['string','required'],
            'content'=>['string','required'],
            'link'=>['url','required'],
        ]);

        if (isset($data['link']) && $data['link'][0] != null){
                if (preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#",$data['link'],$matches)) {
                    preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#", $data['link'], $matches);
                    if (isset($matches[0][0])) {
                        $data['link']     = $matches[0][0];
                    }
                }else{
                    session()->flash('error','عذراً، من فضلك قم بالتأكد من جميع روابط اليوتيوب التي أدخلتها..');
                    return back();
                }

        }
        $program->stories()->create($data);

        session()->flash('success','تم حفظ العنصر بنجاح..');
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SuccessStory  $successStory
     * @return \Illuminate\Http\Response
     */
    public function show(SuccessStory $successStory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SuccessStory  $successStory
     * @return \Illuminate\Http\Response
     */
    public function edit(SuccessStory $successStory)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SuccessStory  $successStory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SuccessStory $story)
    {

        $data = $request->validate([
            'title'=>['string','required'],
            'content'=>['string','required'],
            'link'=>['url','required'],
        ]);

        if (isset($data['link']) && $data['link'][0] != null){
            if (preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#",$data['link'],$matches)) {
                preg_match_all("#(?<=v=|v\/|vi=|vi\/|youtu.be\/)[a-zA-Z0-9_-]{11}#", $data['link'], $matches);
                if (isset($matches[0][0])) {
                    $data['link']     = $matches[0][0];
                }
            }else{
                session()->flash('error','عذراً، من فضلك قم بالتأكد من جميع روابط اليوتيوب التي أدخلتها..');
                return back();
            }
        }
        $story->update([
            'title'=>$data['title'],
            'content'=>$data['content'],
            'link'=>$data['link'],
        ]);

        session()->flash('success','تم تحديث بيانات العنصر بنجاح..');
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SuccessStory  $successStory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SuccessStory $story)
    {
        $story->delete();
        session()->flash('success','تم حذف العنصر بنجاح..');
        return back();
    }
}
