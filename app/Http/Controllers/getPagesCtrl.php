<?php

namespace App\Http\Controllers;

use App\Activity;
use App\Faq;
use App\Program;
use Illuminate\Http\Request;

class getPagesCtrl extends Controller
{
    public function program($slug)
    {
        $item = Program::where('slug',$slug)->with(['activities','stories','helps'])->first();
        $item->visit +=1;
        $item->save();
        return view('frontend.pages.program',compact('item'));
    }
    public function site_activities()
    {
        $data = Activity::orderByDesc('id')->paginate();
        return view('frontend.pages.site_activities',compact('data'));
    }
    public function site_activities_details($activity)
    {
        $item = Activity::where('slug',$activity)->first();

        $item ->visit +=1;
        $item->save();
        return view('frontend.pages.activityDetails',compact('item'));
    }
    public function faq()
    {
        $data = Faq::all();
        return view('frontend.pages.faq',compact('data'));
    }
}
