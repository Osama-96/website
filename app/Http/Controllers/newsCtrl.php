<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;
use Cviebrock\EloquentSluggable\Services\SlugService;


class newsCtrl extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['SiteIndex','showForUser']]);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = News::orderByDesc('id')->paginate(10);
        return view('backend.pages.news.index',compact('data'));
    }
    public function SiteIndex()
    {
        $data = News::orderByDesc('id')->paginate(9);
        return view('frontend.pages.newsPage',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $data = $request->validate([
           'title'=>['required','max:250','string'],
           'content'=>['required','string'],
           'file'=>['required','file','mimes:jpeg,jpg,png'],
        ]);



        $image = $this->uploadImage($request,'file','news');
        $new = New News();

        $new->title = $data['title'];
        $new->content = $data['content'];
        $new->image = $image;
        $new->save();
        session()->flash('success','تم حفظ الخبر الجديد بنجاح، شكراً لك!');
        return  back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $news = News::where('slug',$slug)->first();
        if(!$news){
            return abort(404);
        }
        return view('backend.pages.news.show',compact('news'));
    }
    public function showForUser($slug)
    {
        $item = News::where('slug',$slug)->first();

        if(!$item){
            return abort(404);
        }
        $item->visit +=1;
        $item->save();
        return view('frontend.pages.newsDetails',compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($slug)
    {
        $news = News::where('slug',$slug)->first();
        return view('backend.pages.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $slug)
    {
        $data = $request->validate([
           'title'=>['required','max:250', 'string'] ,
            'content'=>['required','string'],
            'file'=>['nullable','file','mimes:jpeg,jpg,png'],
        ]);
        $news = News::where('slug',$slug)->first();
        if($request->hasFile('file')){
            \File::Delete($news->image);
            $image = $this->uploadImage($request,'file','news');
            $news->image = $image;
        }
        $news->title = $data['title'];
        $news->content = $data['content'];
        $news->save();
        return redirect()->route('News_Show',$news);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($slug)
    {

        $news = News::where('slug',$slug)->first();
        \File::Delete($news->image);
        $news->delete();
        session()->flash('success','تم حذف العنصر بنجاح!');
        return redirect()->route('news.index');

    }
}
