<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreHelpRequiest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'nullable|max:750',
            'file' => 'image|mimes:jpeg,jpg,png',
        ];
    }
    public function messages()
    {
        return [
            'text.max' => 'يجب ألا يزيد النص عن 750 حرف',
            'file.image' => 'لا بد وأن يكون الملف صورة',
            'file.mimes' => 'لابد وأن تكون الصورة بإحدى هذه الصيغ jpeg,jpg,png',
        ];
    }
}
