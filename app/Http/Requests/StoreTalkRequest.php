<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTalkRequest extends FormRequest
{ /**
 * Determine if the user is authorized to make this request.
 *
 * @return bool
 */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|max:400',
            'file' => 'required|image|mimes:jpeg,jpg,png',
            'name' => 'required|string',
            'job' => 'required|string',
        ];
    }
    public function messages()
    {
        return [
            'text.max' => 'يجب ألا يزيد النص عن 400 حرف',
            'text.required' => 'يجب إدخال الكلام الذي تم قوله',
            'file.required' => 'لا بد من رفع صورة القائل',
            'name.required' => 'لا بد من إدخال اسم القائل',
            'name.string' => ' لا بد من أن يكون اسم القائل نصاً',
            'job.required' => 'لا بد من إدخال وظيفة القائل',
            'job.string' => 'لا بد من أن يكون وظيفة القائل نصاً ',
            'file.image' => 'لا بد وأن يكون الملف صورة',
            'file.mimes' => 'لابد وأن تكون الصورة بإحدى هذه الصيغ jpeg,jpg,png',
        ];
    }
}
