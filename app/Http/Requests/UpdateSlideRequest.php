<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSlideRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text'=>'nullable|string|max:250',
            'link'=>'nullable|url',
            'file'=>'sometimes|image|mimes:jpeg,jpg,png|max:1000',
        ];
    }
    public function messages()
    {
        return [
            'text.max' => 'يجب ألا يزيد النص عن 250 حرف',
            'link.url' => 'يجب أن يكون اللينك صحيحاً مثل (https://www.facebook.com)',
            'file.image' => 'لا بد وأن يكون الملف صورة',
            'file.mimes' => 'لابد وأن تكون الصورة بإحدى هذه الصيغ jpeg,jpg,png',
            'file.required' => 'يجب رفع الصورة..',
            'file.max' => 'يجب ألا يزيد حجم الصورة عن 1 ميجا..',
        ];
    }
}
