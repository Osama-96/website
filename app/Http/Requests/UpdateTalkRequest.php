<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTalkRequest extends FormRequest
{ public function authorize()
{
    return true;
}

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'text' => 'required|max:400',
            'file' => 'nullable|image|mimes:jpeg,jpg,png',
            'name' => 'required|string',
            'job' => 'required|string',
        ];
    }
    public function messages()
    {
        return [
            'text.max' => 'يجب ألا يزيد النص عن 400 حرف',
            'text.required' => 'يجب إدخال الكلام الذي تم قوله',
            'name.required' => 'لا بد من إدخال اسم القائل',
            'name.string' => ' لا بد من أن يكون اسم القائل نصاً',
            'job.required' => 'لا بد من إدخال وظيفة القائل',
            'job.string' => 'لا بد من أن يكون وظيفة القائل نصاً ',
            'file.image' => 'لا بد وأن يكون الملف صورة',
            'file.mimes' => 'لابد وأن تكون الصورة بإحدى هذه الصيغ jpeg,jpg,png',
        ];
    }
}
