<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class postContactRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'=>'required|string',
            'phone'=>'required|string',
            'reason'=>'required|string',
            'message'=>'required|string',
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'الاسم مطلوب',
            'phone.required' => 'الهاتف مطلوب',
            'reason.required' => 'الغرض مطلوب',
            'message.required' => 'الرسالة مطلوبة',
            'name.string' => ' لا بد من أن يكون الاسم نصاً',
            'phone.string' => ' لا بد من أن يكون رقم الهاتف صحيحاً',
            'reason.string' => 'اختر أحد الأغراض',
            'message.string' => 'قم بإدخال نص صحيح',

        ];
    }
}
