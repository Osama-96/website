<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class NeedHelp extends Model
{
    protected $fillable = [
        'text',
        'file',
        'programmable_id',
        'programmable_type',
    ];

    use SoftDeletes;
    public function programmable()
    {
        return $this->morphTo();
    }
}
