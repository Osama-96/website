<?php

namespace App;

use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{

    protected $fillable = [
        'title',
        'content',
        'slug',
        'visit',
        'image',
    ];
    use HasSlug,SoftDeletes;
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['title', 'created_at'])
            ->saveSlugsTo('slug')
            ->usingSeparator('_')
            ->usingLanguage('ar');

    }
    public function getRouteKeyName()
    {
        return 'slug';
    }

}
