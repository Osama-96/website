<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Sluggable\HasSlug;
use Spatie\Sluggable\SlugOptions;

class Program extends Model
{
    protected $fillable = [
        'title',
        'content',
        'slug',
        'visit',
        'story_sub_title',
        'story_text',
        'can_help_sub_title',
    ];

    use HasSlug,SoftDeletes;
    public function getSlugOptions() : SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom(['title', 'created_at'])
            ->saveSlugsTo('slug')
            ->usingSeparator('_')
            ->usingLanguage('ar');
    }
    public function getRouteKeyName()
    {
        return 'slug';
    }

    //relationships
    public function activities()
    {
        return $this->morphMany(Activity::class , 'programmable');
    }

    public function stories()
    {
        return $this->morphMany(SuccessStory::class , 'programmable');
    }

    public function helps()
    {

        return $this->morphMany(NeedHelp::class , 'programmable');
    }


}
