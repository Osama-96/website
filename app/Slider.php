<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Slider extends Model
{
    protected $fillable = [
        'text',
        'link',
        'file',

    ];


    use SoftDeletes;

}
