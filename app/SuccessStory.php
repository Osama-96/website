<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SuccessStory extends Model
{
    protected $fillable = [
        'title',
        'content',
        'link',
        'programmable_id',
        'programmable_Type',
    ];

    use SoftDeletes;

    public function programmable()
    {
        return $this->morphTo(Program::class);
    }
}
