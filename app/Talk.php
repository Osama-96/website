<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Talk extends Model
{
    protected $fillable = [
        'text',
        'file',
        'name',
        'job',
    ];

    use SoftDeletes;
}
