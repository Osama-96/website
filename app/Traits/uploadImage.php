<?php
    namespace App\Traits;
    use Illuminate\Http\Request;
    trait uploadImage{
        protected function uploadImage(Request $request,string $inputName='file',string $path='uploads',array $validation=[]){
            // remove any / char form var
            $path='uploads/'.rtrim($path,'/');
            //dd($request->all());
            // validate Image
            if (empty($validation)) $request->validate([$inputName=>['sometimes','mimes:jpeg,jpg,png']]);
            else $request->validate([$inputName=>$validation]);


            // check has file
            if ($request->hasFile($inputName)){
                $image=$request->file($inputName);
                $filename=uniqid('', true).'.'.date('Y.m.d.H.i.s').'.'.$image->getClientOriginalExtension();
                $image->move(public_path().'/'.rtrim($path,'/'),$filename);
                return $path.'/'.$filename;
            }
            // default Image
            return null;
        }
        protected function uploadMultiImages(Request $request,string $inputName='file',string $path='uploads',array $validation=[]){
            // remove any / char form var
            $path='uploads/'.rtrim($path,'/');
            $images = [];
            //dd($request->all());
            // validate Image
            if (empty($validation)) $request->validate([$inputName.'*'=>['nullable','mimes:jpeg,jpg,png']]);
            else $request->validate([$inputName=>$validation]);


            // check has file
            if ($request->hasFile($inputName)){
                foreach($request->file($inputName) as $key=> $image)
                {
                    $filename=uniqid('', true).'.'.date('Y.m.d.H.i.s').'.'.$image->getClientOriginalExtension();
                    $image->move(public_path().'/'.rtrim($path,'/'),$filename);
                    $images[$key] = $path.'/'.$filename;
                }
                return $images;
            }
            // default Image
            return null;
        }
    }


