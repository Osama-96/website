<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activities', function (Blueprint $table) {
            $table->id();
            $table->text('slug');
            $table->text('title');
            $table->bigInteger('programmable_id')->nullable();
            $table->string('programmable_type')->nullable();
            $table->text('images')->nullable();
            $table->text('videos')->nullable();
            $table->longText('content')->nullable();
            $table->bigInteger('visit')->default(0);
            $table->timestamps();
            $table->softDeletesTz();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activities');
    }
}
