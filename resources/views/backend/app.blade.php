<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Masr_Elmahrousa | @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- favicon
		============================================ -->
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.png')}}">
    <!-- Google Fonts
		============================================ -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/bootstrap.min.rtl.css')}}">
    <!-- Bootstrap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/font-awesome.min.css')}}">
    <!-- owl.carousel CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/owl.carousel.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/owl.theme.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/owl.transitions.css')}}">
    <!-- meanmenu CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/meanmenu/meanmenu.min.css')}}">
    <!-- animate CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/animate.css')}}">
    <!-- normalize CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/normalize.css')}}">
    <!-- mCustomScrollbar CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/scrollbar/jquery.mCustomScrollbar.min.css')}}">
    <!-- jvectormap CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/jvectormap/jquery-jvectormap-2.0.3.css')}}">
    <!-- notika icon CSS
       ============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/notika-custom-icon.css')}}">
    <!-- dropzone CSS
          ============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/dropzone/dropzone.css')}}">
    <!-- summernote CSS
      ============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/summernote/summernote.css')}}">

    <!-- wave CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/wave/waves.min.css')}}">
    <link rel="stylesheet" href="{{asset('backend/css/wave/button.css')}}">

    <!-- main CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/main.css')}}">
    <!-- style CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/style.css')}}">
    <!-- responsive CSS
		============================================ -->
    <link rel="stylesheet" href="{{asset('backend/css/responsive.css')}}">
    <!-- modernizr JS
		============================================ -->
    <script src="{{asset('backend/js/vendor/modernizr-2.8.3.min.js')}}"></script>
    <style>
        @font-face {
            font-family: kufi;
            src: url({{asset('fonts/ArbFONTS-Droid.Arabic.Kufi_DownloadSoftware.iR_.ttf')}});
        }
        *{
            font-family: kufi;
        }
        ul.notika-menu-wrap li a{
            padding: unset;
            padding: 15px;
        }
    </style>
    @stack('css')
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<!-- Start Header Top Area -->
@include('backend.includes.header')
<!-- End Header Top Area -->
<!-- Mobile Menu start -->
@include('backend.includes.menu')
<!-- Main Menu area End-->

@if ($errors->any())
    <div class="alert-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible alert-mg-b-0" role="alert" style="text-align: right; direction: rtl">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button>  حدث خطأ ما! {{$error}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endif
@if(session()->has('success') || session()->has('error'))
    <div class="alert-area">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    @if(session()->has('error'))
                    <div class="alert alert-danger alert-dismissible alert-mg-b-0" role="alert" style="text-align: right">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> لم تتم العملية | {{session()->get('error')}}
                    </div>
                    @elseif(session()->has('success'))
                    <div class="alert alert-success alert-dismissible" role="alert"  style="text-align: right">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true"><i class="notika-icon notika-close"></i></span></button> عملية ناجحة | {{session()->get('success')}}
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endif

@yield('content')

<!-- Start Footer area-->
<div class="footer-copyright-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="footer-copy-right">
                    <p>الحقوق محفوظة © {{date('Y')}}
                        جمعية مصر المحروسة
                         <a href="https://colorlib.com" style="display: none"> </a>.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Footer area-->
<!-- jquery
    ============================================ -->
<script src="{{asset('backend/js/vendor/jquery-1.12.4.min.js')}}"></script>
<!-- bootstrap JS
    ============================================ -->
<script src="{{asset('backend/js/bootstrap.min.js')}}"></script>
<!-- wow JS
    ============================================ -->
<script src="{{asset('backend/js/wow.min.js')}}"></script>
<!-- price-slider JS
    ============================================ -->
<script src="{{asset('backend/js/jquery-price-slider.js')}}"></script>
<!-- owl.carousel JS
    ============================================ -->
<script src="{{asset('backend/js/owl.carousel.min.js')}}"></script>
<!-- scrollUp JS
    ============================================ -->
<script src="{{asset('backend/js/jquery.scrollUp.min.js')}}"></script>
<!-- meanmenu JS
    ============================================ -->
<script src="{{asset('backend/js/meanmenu/jquery.meanmenu.js')}}"></script>
<!-- counterup JS
    ============================================ -->
<script src="{{asset('backend/js/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('backend/js/counterup/waypoints.min.js')}}"></script>
<script src="{{asset('backend/js/counterup/counterup-active.js')}}"></script>
<!-- mCustomScrollbar JS
    ============================================ -->
<script src="{{asset('backend/js/scrollbar/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<!-- jvectormap JS
    ============================================ -->
<script src="{{asset('backend/js/jvectormap/jquery-jvectormap-2.0.2.min.js')}}"></script>
<script src="{{asset('backend/js/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('backend/js/jvectormap/jvectormap-active.js')}}"></script>
<!-- sparkline JS
    ============================================ -->
<script src="{{asset('backend/js/sparkline/jquery.sparkline.min.js')}}"></script>
<script src="{{asset('backend/js/sparkline/sparkline-active.js')}}"></script>
<!-- sparkline JS
    ============================================ -->
<script src="{{asset('backend/js/flot/jquery.flot.js')}}"></script>
<script src="{{asset('backend/js/flot/jquery.flot.resize.js')}}"></script>
<script src="{{asset('backend/js/flot/curvedLines.js')}}"></script>
<script src="{{asset('backend/js/flot/flot-active.js')}}"></script>
<!-- knob JS
    ============================================ -->
<script src="{{asset('backend/js/knob/jquery.knob.js')}}"></script>
<script src="{{asset('backend/js/knob/jquery.appear.js')}}"></script>
<script src="{{asset('backend/js/knob/knob-active.js')}}"></script>

<!-- summernote JS
      ============================================ -->
<script src="{{asset('backend/js/summernote/summernote-updated.min.js')}}"></script>
<script src="{{asset('backend/js/summernote/summernote-active.js')}}"></script>
<!-- dropzone JS
    ============================================ -->
<script src="{{asset('backend/js/dropzone/dropzone.js')}}"></script>

<!--  wave JS
    ============================================ -->
<script src="{{asset('backend/js/wave/waves.min.js')}}"></script>
<script src="{{asset('backend/js/wave/wave-active.js')}}"></script>
<!--  todo JS
    ============================================ -->
<script src="{{asset('backend/js/todo/jquery.todo.js')}}"></script>
<!-- plugins JS
    ============================================ -->
<script src="{{asset('backend/js/plugins.js')}}"></script>

<!-- main JS
    ============================================ -->
<script src="{{asset('backend/js/main.js')}}"></script>

<script>

    $.ajaxSetup({
        headers:{
            'X-CSRF-TOKEN':$('meta[name="csrf-token"]').attr('content')
        }
    });



    function readURL(input,name='#imgPreview') {
        if (input.files && input.files[0]) {
            let reader = new FileReader();
            reader.onload = function(e) {
                $(name).attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $('form').submit(function(){
        $(this).prop("disabled", true);
    });
</script>
@stack('js')

</body>

</html>
