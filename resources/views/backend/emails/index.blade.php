@extends('backend.app')
@section('title') contact Us @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>رسائل العملاء</h2>
                                        <p>  جميع الرسائل </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="alert-area">
        <div class="container">
            @if(count($data) == 0 )
                <div class="row" >
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <p class="alert alert-danger text-center text-danger "> لم يتم حفظ أي بيانات بعد!!</p>
                    </div>
                </div>
            @endif
            @foreach($data as  $key=>$row)
            <div class="row rounded " style="padding: 20px; margin: 20px 0 ;  background-color:@if($row->seen ==0) #F44336 @else white  @endif; text-align: right;">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 "  style="direction: rtl; color:{{($row->seen ==0)?'white':''}} ">
                    <h3 >{{$row->name}} <span style="font-size: 13px; margin: 0 50px; ">{{$row->created_at->toDayDateTimeString()}}</span></h3>
                    <h5>{{$row->reason}}</h5>

                    <table class="table table-responsive table-bordered " >
                        <tr class="bg-success">
                            <th> الغرض</th>
                            <th>الهاتف</th>
                       </tr>

                        <tr>
                            <td>{{$row->for??'لا يوجد'}}</td>
                            <td>{{$row->phone}}</td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <p>{{$row->message}}</p>
                            </td>
                        </tr>
                    </table>


                </div>



                <div class=" col-md-12 material-design-btn" style="margin-top: 30px; direction: rtl">
                    @if($row->seen ==0)
                        <form action="{{route('mails.update',$row)}}" method="POST">
                        @csrf
                        @method('PUT')

                        <button type="submit" class="btn notika-btn-green btn-reco-mg btn-button-mg">تحديد كمقروء</button>

                    </form>
                    @else
                        <small >تمت القراءة في: {{$row->updated_at->toDayDateTimeString()}} </small>
                    @endif
                </div>

            </div>
            @endforeach
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" >
                    {{$data->onEachSide(3)->links()}}
                </div>
            </div>
        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
    </script>
@endpush
