<div class="header-top-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" style="float: left">
                <div class="logo-area">
                    <a href="{{route('/')}}"><img src="{{asset('assets/images/logo.png')}}" width="50" alt="" /></a>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="float: left">
                <div class="header-top-menu">
                    <ul class="nav navbar-nav notika-top-nav">
                        <li class="nav-item">
                            <a href="javascript:void(0);" onclick="document.getElementById('logout').submit();"><span><i class="fa fa-sign-out "></i></span></a>
                        </li>
                        <form action="{{route('logout')}}" id="logout" method="post" class="hidden">
                            @csrf
                        </form>

                        <li class="nav-item">
                            <a href="{{route('settings')}}" ><span><i class="notika-icon notika-settings"></i></span></a>
                        </li>

                        <li class="nav-item dropdown">
                            <a href="#" data-toggle="dropdown" role="button" aria-expanded="false" class="nav-link dropdown-toggle">
                                <span><i class="notika-icon notika-mail"></i></span>
                                <div class="spinner4 spinner-4"></div><div class="ntd-ctn"><span>{{\App\ContactUs::where('seen',0)->count()}}</span></div>
                            </a>
                            <div role="menu" class="dropdown-menu message-dd animated zoomIn">
                                <div class="hd-mg-tt">
                                    <h2>  الرسائل الجديدة</h2>
                                </div>
                                <div class="hd-message-info">
                                    @if(!\App\ContactUs::where('seen',0)->count())
                                        <a href="#">
                                            <div class="hd-message-sn">
                                                <div class="hd-message-img">
                                                    <img src="{{asset('backend/images/noImage.png')}}" width="100"  height="100" alt="" />
                                                </div>
                                                <div class="hd-mg-ctn">

                                                    <p style="color: #dddddd">لا توجد رسائل جديدة</p>
                                                </div>
                                            </div>
                                        </a>
                                    @endif
                                    @foreach(\App\ContactUs::where('seen',0)->get() as $key=>$item)
                                    <a href="#">
                                        <div class="hd-message-sn">
                                            <div class="hd-message-img">
                                                <img src="{{asset('1.png')}}" width="100px"  height="100px"alt="" />
                                            </div>
                                            <div class="hd-mg-ctn">
                                                <h3>{{$item->name}}</h3>
                                                <p style="color: #dddddd">{{$item->created_at->diffForHumans()}}</p>
                                            </div>
                                        </div>
                                    </a>
                                    @endforeach

                                </div>
                                <div class="hd-mg-va">
                                    <a href="{{route('mails.index')}}">رؤية الكل</a>
                                </div>
                            </div>
                        </li>


                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
