<div class="mobile-menu-area">
    <div class="container" >
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="mobile-menu">
                    <nav id="dropdown" >
                        <ul class="mobile-menu-nav" >
                            <li><a  href="{{route('admin')}}">الرئيسية</a></li>
                            <li><a data-toggle="collapse" data-target="#demoevent" href="#"> أخبار الجمعية</a>
                                <ul id="demoevent" class="collapse dropdown-header-top">
                                    <li><a href="{{route('newNews')}}">حفظ خبر جديد</a></li>
                                    <li><a href="{{route('news.index')}}">عرض جميع الأخبار</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#democrou" href="#">البرامج</a>
                                <ul id="democrou" class="collapse dropdown-header-top">
                                    <li><a href="{{route('programs.create')}}">إنشاء برنامج جديد</a></li>
                                    <li><a  href="{{route('programs.index')}}">عرض كل البرامج</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#democrou2" href="#">الأنشطة</a>
                                <ul id="democrou2" class="collapse dropdown-header-top">
                                    <li><a href="{{route('activities.create')}}">إنشاء نشاط جديد</a></li>
                                    <li><a  href="{{route('activities.index')}}">عرض كل الأنشطة</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#democrou3" href="#">سلايدر</a>
                                <ul id="democrou3" class="collapse dropdown-header-top">
                                    <li><a href="{{route('slides.create')}}">إنشاء سلايدر جديد</a></li>
                                    <li><a  href="{{route('slides.index')}}">عرض السلايدرز</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#democrou3" href="#"> قالوا عنا</a>
                                <ul id="democrou3" class="collapse dropdown-header-top">
                                    <li><a href="{{route('talks.create')}}">حفظ مقولة جديدة</a></li>
                                    <li><a  href="{{route('talks.index')}}">عرض كل المقولات</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#demolibra" href="#">الأسئلة الشائعة</a>
                                <ul id="demolibra" class="collapse dropdown-header-top">
                                    <li><a href="{{route('faqs.index')}}">عرض كل الأسئلة</a></li>
                                    <li><a href="{{route('faqs.create')}}">حفظ سؤال جديد</a></li>
                                </ul>
                            </li>
                            <li><a data-toggle="collapse" data-target="#edumesg" href="#">رسائل تربوية</a>
                                <ul id="edumesg" class="collapse dropdown-header-top">
                                    <li><a href="{{route('edumesgs.index')}}">عرض كل الرسائل التربوية</a></li>
                                    <li><a href="{{route('edumesgs.create')}}">حفظ رسالة جديدة</a></li>
                                </ul>
                            </li>

                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Mobile Menu end -->

<!-- Main Menu area start-->
<div class="main-menu-area mg-tb-40">
    <div class="container" >
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align: right">
                <ul class="nav nav-tabs notika-menu-wrap menu-it-icon-pro" >
                    <li @if(url()->current() == route('admin')) class="active" @endif><a  href="{{route('admin')}}"><i class="notika-icon notika-house"></i> الرئيسية</a>
                    </li>
                    <li @if(url()->current() == route('news.index') || url()->current() == route('newNews')) class="active" @endif><a data-toggle="tab" href="#newsbox"><i class="notika-icon notika-menus"></i> أخبار الجمعية</a>
                    </li>
                    <li @if(url()->current() == route('programs.index') || url()->current() == route('programs.create'))  class="active" @endif><a data-toggle="tab" href="#mailbox"><i class="notika-icon notika-mail"></i> البرامج</a>
                    </li>
                    <li @if(url()->current() == route('activities.index') || url()->current() == route('activities.create'))  class="active" @endif><a data-toggle="tab" href="#Interface"><i class="notika-icon notika-edit"></i> الأنشطة</a>
                    </li>
                    <li @if(url()->current() == route('slides.index') || url()->current() == route('slides.create'))  class="active" @endif><a data-toggle="tab" href="#Charts"><i class="notika-icon notika-bar-chart"></i> سلايدر </a>
                    </li>
                    <li @if(url()->current() == route('talks.index') || url()->current() == route('talks.create'))  class="active" @endif><a data-toggle="tab" href="#talks"><i class="notika-icon notika-chat"></i> قالوا عنا </a>
                    </li>
                    <li @if(url()->current() == route('faqs.index') || url()->current() == route('faqs.create'))  class="active" @endif><a data-toggle="tab" href="#Tables"><i class="notika-icon notika-windows"></i> الأسئلة الشائعة</a>
                    </li>
                    <li @if(url()->current() == route('edumesgs.index') || url()->current() == route('edumesgs.create'))  class="active" @endif><a  data-toggle="tab" href="#edu"><i class="notika-icon notika-house"></i> رسائل تربوية </a>
                    </li>
                </ul>
                <div class="tab-content custom-menu-content">

                    <div id="newsbox" class="tab-pane notika-tab-menu-bg animated flipInX
                        @if(url()->current() == route('news.index') || url()->current() == route('newNews')) active @endif">
                        <ul class="notika-main-menu-dropdown">
                            <li><a href="{{route('newNews')}}">حفظ خبر جديد</a>
                            </li>
                            <li><a href="{{route('news.index')}}">عرض جميع الأخبار</a>
                            </li>

                        </ul>
                    </div>
                    <div id="mailbox" class="tab-pane notika-tab-menu-bg animated flipInX
                         @if(url()->current() == route('programs.index') || url()->current() == route('programs.create')) active @endif">
                        <ul class="notika-main-menu-dropdown">
                            <li><a href="{{route('programs.create')}}">إنشاء برنامج جديد</a>
                            </li>
                            <li><a href="{{route('programs.index')}}">عرض كل البرامج</a>
                            </li>

                        </ul>
                    </div>
                    <div id="Interface" class="tab-pane notika-tab-menu-bg animated flipInX
                        @if(url()->current() == route('activities.index') || url()->current() == route('activities.create')) active @endif">
                        <ul class="notika-main-menu-dropdown">

                            <li><a href="{{route('activities.create')}}">حفظ نشاط جديد</a>
                            </li>
                            <li><a href="{{route('activities.index')}}">عرض كل الأنشطة</a>
                            </li>
                        </ul>
                    </div>
                    <div id="Charts" class="tab-pane notika-tab-menu-bg animated flipInX
                        @if(url()->current() == route('slides.index') || url()->current() == route('slides.create')) active @endif">
                        <ul class="notika-main-menu-dropdown">
                            <li><a href="{{route('slides.index')}}">عرض السلايدرز</a>
                            </li>
                            <li><a href="{{route('slides.create')}}">حفظ سلايد جديد</a>
                            </li>
                        </ul>
                    </div>
                    <div id="talks" class="tab-pane notika-tab-menu-bg animated flipInX
                        @if(url()->current() == route('talks.index') || url()->current() == route('talks.create')) active @endif">
                        <ul class="notika-main-menu-dropdown">
                            <li><a href="{{route('talks.index')}}">عرض كل المقولات</a>
                            </li>
                            <li><a href="{{route('talks.create')}}">حفظ مقولة جديدة</a>
                            </li>
                        </ul>
                    </div>
                    <div id="Tables" class="tab-pane notika-tab-menu-bg animated flipInX
                       @if(url()->current() == route('faqs.index') || url()->current() == route('faqs.create')) active @endif">
                        <ul class="notika-main-menu-dropdown">
                            <li><a href="{{route('faqs.index')}}">عرض كل الأسئلة</a>
                            </li>
                            <li><a href="{{route('faqs.create')}}">حفظ سؤال جديد</a>
                            </li>
                        </ul>
                    </div>
                    <div id="edu" class="tab-pane notika-tab-menu-bg animated flipInX
                     @if(url()->current() == route('edumesgs.index') || url()->current() == route('edumesgs.create'))  active @endif">
                        <ul class="notika-main-menu-dropdown">
                            <li><a href="{{route('edumesgs.index')}}">عرض كل الرسائل التربوية</a>
                            </li>
                            <li><a href="{{route('edumesgs.create')}}">حفظ رسالة جديدة</a>
                            </li>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
