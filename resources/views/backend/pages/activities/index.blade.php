@extends('backend.app')
@section('title') New News @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>أنشطة الجمعية</h2>
                                        <p>  جميع الأنشطة ({{\App\Activity::count()}})</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="alert-area">
        <div class="container">
            @if(count($data) == 0 )
                <div class="row" >
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <p class="alert alert-danger text-center text-danger "> لم يتم حفظ أي بيانات بعد!!</p>
                    </div>
                </div>
            @endif
            @foreach($data as  $key=>$row)
            <div class="row rounded " style="padding: 20px; margin: 20px 0 ; background-color: white; text-align: right;">

                <div class="col-lg-9 col-md-8 col-sm-12 col-xs-12" style="direction: rtl">
                    <h4 >{{$row->title}}</h4>
                    <p>{{substrwords(strip_tags($row->content),200)}}</p>
                    <table class="table table-responsive table-bordered">
                        <tr class="bg-success">
                            <th>البرنامج</th>
                            <th>تاريخ الإنشاء</th>
                            <th>عدد المشاهدات</th>
                            <th>عدد الصور</th>
                            <th>عدد الفديوهات</th>
                       </tr>

                        <tr>
                            <td>{{$row->programmable->title??'لا يوجد'}}</td>
                            <td>{{$row->created_at->toDayDateTimeString()}}</td>
                            <td>{{$row->visit}}</td>
                            <td>{{($row->images)?count($row->images) : 0}}</td>
                            <td>{{($row->videos)?count($row->images):0}}</td>
                        </tr>
                    </table>
                </div>

                <div class="col-lg-3 col-md-4 col-sm-12 col-xs-12" style="text-align: center;">
                        @if(isset($row->images))
                            <img src="{{asset($row->images[0])}}"  class="media-object" alt="صورة النشاط" >
                        @else
                            <img src="{{asset('backend/images/noImage.png')}}"  class="media-object" alt="" >
                            <strong style="color: #dddddd">لم يتم حفظ أي صور</strong>
                        @endif
                </div>

                <div class=" col-md-12 material-design-btn" style="margin-top: 30px;">
                    <a href="{{route('activities.show',$row)}}" class="btn notika-btn-green btn-reco-mg btn-button-mg">عرض</a>
                </div>

            </div>
            @endforeach
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" >
                    {{$data->onEachSide(3)->links()}}
                </div>
            </div>
        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
    </script>
@endpush
