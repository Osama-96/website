@extends('backend.app')
@section('title')  F&Q @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>الأسئلة الشائعة</h2>
                                        <p> {{$item->title}} </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="alert-area">
        <div class="container">

            <div class="row rounded " style="padding: 20px; margin: 20px 0 ; background-color: white; text-align: right;">
                <div class="col-md-12" >
                    <h4 style="margin: 25px 0; text-align: right;" >{{$item->title}}</h4>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="direction: rtl; margin-top: 50px;">

                    <h6> الجواب:</h6>
                    <div class=" mt-5" style="margin-bottom: 30px;">
                    {!! $item->content !!}
                    </div>

                    <div class="col-md-12">
                        <table class="table table-responsive table-bordered "style="margin-top: 50px;">
                            <tr>
                                <th>تاريخ الإنشاء</th>
                                <td>{{$item->created_at->toDayDateTimeString()}}</td>
                            </tr>
                        </table>
                        <div class="material-design-btn">
                            <a href="{{route('faqs.edit',$item)}}" class="btn notika-btn-bluegray btn-reco-mg btn-button-mg">تعديل</a>
                            <button data-toggle="modal" data-target="#myModalfive" class="btn notika-btn-red btn-reco-mg btn-button-mg">حذف</button>
                            <div class="modal animated flash" id="myModalfive" role="dialog">
                                <div class="modal-dialog modals-default nk-red">
                                    <form action="{{route('faqs.destroy',$item)}}" method="post" >
                                        @method('DELETE')
                                        @csrf

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                <h2>حذف العنصر</h2>
                                                <p>من فضلك قم بتأكيد عملية الحذف..</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-default" style="display: inline-block">حذف</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
        $('body').on('click','.frame',function() {
            video = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" style="height:100% " src="' + $(this).attr('data-video') + '" allowfullscreen></div>';
            $(this).replaceWith(video);
        });
    </script>
@endpush
