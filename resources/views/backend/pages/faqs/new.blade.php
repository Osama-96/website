@extends('backend.app')
@section('title') New F&Q @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>أخبار الجمعية</h2>
                                        <p> إنشاء سؤال جديد </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="invoice-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="invoice-wrap">
                        <div class="invoice-img">
                            <img src="{{asset('assets/images/logo.png')}}" alt="" />
                        </div>
                        <div class="invoice-hds-pro">
                            <form action="{{route('faqs.store')}}" method="post" enctype="multipart/form-data">
                                @csrf()
                                <div class="row" dir="rtl">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
                                        <div class="view-mail-list sm-res-mg-t-30">
                                            <div class="view-mail-hd">
                                                <div class="view-mail-hrd">
                                                    <h2>سؤال جديد</h2>
                                                </div>
                                            </div>
                                            <div class="cmp-int mg-t-20">
                                                <div class="row">
                                                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                        <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                            <span>السؤال: </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                <input type="text" name="title" class="form-control" placeholder="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                        <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                            <span>الجواب: </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="cmp-int-box mg-t-20">
                                                                <textarea id="summernote" class="form-control" style="height: 200px;"  name="content"></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <div class="vw-ml-action-ls text-right mg-t-20" style="text-align: center" >
                                                <div class="btn-group ib-btn-gp active-hook nk-email-inbox text-center" >
                                                    <button class="btn btn-default btn-sm waves-effect"><i class="notika-icon notika-checked"></i> حفظ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 200,
            });
        });

        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID

            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class=""><input type="url" class="form-control" style="margin-top: 20px;"  name="youtubeLink[]"  placeholder="ادخل لينك يوتيوب.." required /><a href="#" class="remove_field btn btn-xs btn-danger" style="margin-top: 10px">حذف</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
        $(document).ready(function() {
            var max_fields      = 10; //maximum input boxes allowed
            var wrapper   		= $(".input_images_wrap"); //Fields wrapper
            var add_button      = $(".add_image_button"); //Add button ID


            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                var number          = $(".input_images_wrap input").length;
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    x++; //text box increment
                    $(wrapper).append('<div class="col-md-4"> <img class="rounded" width="400" height="260" style="height: 260px" id="imgPreview'+number+'" src="" alt=""> <input type="file" class="form-control-file"  name="file[]"  onchange="readURL(this,\'#imgPreview'+number+'\')"  required /> <a href="#" class="remove_image btn btn-xs btn-danger" style="margin-top: 10px">حذف</a></div>'); //add input box
                }
            });

            $(wrapper).on("click",".remove_image", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
    </script>
@endpush
