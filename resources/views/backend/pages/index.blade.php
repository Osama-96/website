@extends('backend.app')
@section('title') dashboard @stop
@section('content')

    <!-- Start Status area -->
    <div class="notika-status-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('programs')->count()}}</span></h2>
                            <p>عدد البرامج</p>
                        </div>
                        <div class="sparkline-bar-stats1">1,4,8,6,5,6</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('need_helps')->count()}}</span></h2>
                            <p>عدد الحالات التي تحتاج الدعم</p>
                        </div>
                        <div class="sparkline-bar-stats2">1,4,8,3,3,9,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('success_stories')->count()}}</span></h2>
                            <p>عدد قصص النجاح</p>
                        </div>
                        <div class="sparkline-bar-stats3">4,2,8,2,5,6,3,8</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('activities')->count()}}</span></h2>
                            <p>عدد أنشطة الجمعية</p>
                        </div>
                        <div class="sparkline-bar-stats4">2,7,4,7,3,5,7,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('news')->count()}}</span></h2>
                            <p>عدد أخبار الجمعية</p>
                        </div>
                        <div class="sparkline-bar-stats4">7,4,7,3,5,7,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('edu_mesgs')->count()}}</span></h2>
                            <p>عدد الرسائل التربوية</p>
                        </div>
                        <div class="sparkline-bar-stats4">2,4,8,3,5,7,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('sliders')->count()}}</span></h2>
                            <p>عدد الSlides</p>
                        </div>
                        <div class="sparkline-bar-stats4">4,5,7,4,7,3,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('talks')->count()}}</span></h2>
                            <p>عدد قالو عنا</p>
                        </div>
                        <div class="sparkline-bar-stats4">2,4,8,4,5,7,9</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('faqs')->count()}}</span></h2>
                            <p>عدد الأسئلة الشائعة</p>
                        </div>
                        <div class="sparkline-bar-stats4">2,4,8,4,7,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('news')->sum('visit')}}</span></h2>
                            <p>عدد مشاهدات الأخبار</p>
                        </div>
                        <div class="sparkline-bar-stats4">5,7,4,7,3,5,7,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('activities')->sum('visit')}}</span></h2>
                            <p>عدد مشاهدات الأنشطة</p>
                        </div>
                        <div class="sparkline-bar-stats4">2,4,8,4,3,5,7,5</div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12" style="margin-top:25px;">
                    <div class="wb-traffic-inner notika-shadow sm-res-mg-t-30 tb-res-mg-t-30 dk-res-mg-t-30">
                        <div class="website-traffic-ctn">
                            <h2><span class="counter">{{\DB::table('programs')->sum('visit')}}</span></h2>
                            <p>عدد مشاهدات البرامج</p>
                        </div>
                        <div class="sparkline-bar-stats4">5,7,4,7,3,5,7,5</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End Status area-->

@stop
