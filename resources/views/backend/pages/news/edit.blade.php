@extends('backend.app')
@section('title') New News @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>أخبار الجمعية</h2>
                                        <p> تعديل الخبر | {{$news->title}} </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="invoice-area">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="invoice-wrap">
                        <div class="invoice-img">
                            <img src="{{asset('assets/images/logo.png')}}" alt="" />
                        </div>
                        <div class="invoice-hds-pro">
                            <form action="{{route('news.update',$news)}}" method="post" enctype="multipart/form-data">
                                @method('PUT')
                                @csrf()
                                <div class="row" dir="rtl">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12"  >
                                        <div class="view-mail-list sm-res-mg-t-30">
                                            <div class="view-mail-hd">
                                                <div class="view-mail-hrd">
                                                    <h2>{{$news->title}}</h2>
                                                </div>
                                            </div>
                                            <div class="cmp-int mg-t-20">
                                                <div class="row">
                                                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                        <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                            <span>العنوان: </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                <input type="text" name="title" class="form-control" value="{{$news->title}}" placeholder="" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                        <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                            <span>الصورة: </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                <input type="file" class="form-control" onchange="readURL(this,'#imgPreview')"   name='file' />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="form-group">
                                                            <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                <img class="rounded" width="400" height="260" id="imgPreview" src="{{asset($news->image)}}" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="cmp-int-box mg-t-20">
                                                <textarea id="summernote" name="content">{!! $news->content !!}</textarea>

                                            </div>

                                            <div class="vw-ml-action-ls text-right mg-t-20" style="text-align: center" >
                                                <div class="btn-group ib-btn-gp active-hook nk-email-inbox text-center" >
                                                    <button class="btn btn-default btn-sm waves-effect"><i class="notika-icon notika-checked"></i>  تحديث </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
    </script>
@endpush
