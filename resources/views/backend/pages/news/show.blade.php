@extends('backend.app')
@section('title') New News @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>أخبار الجمعية</h2>
                                        <p> {{$news->title}} </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="alert-area">
        <div class="container">

            <div class="row rounded " style="padding: 20px; margin: 20px 0 ; background-color: white; text-align: right;">
                <div class="col-md-12" style="margin: 25px 0;">
                    <h4 >{{$news->title}}</h4>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" style="direction: rtl">
                    <img src="{{asset($news->image)}}" class="media-object"  style="max-width: 100%" alt="صورة الخبر" >
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="direction: rtl; margin-top: 50px;">

                    <h6> المحتوى:</h6>
                    <div class="text-center mt-5">
                    {!! $news->content !!}
                    </div>
                    <table class="table table-responsive table-bordered "style="margin-top: 50px;">
                        <tr>
                            <th>تاريخ الإنشاء</th>
                            <td>{{$news->created_at->toDayDateTimeString()}}</td>
                        </tr>
                        <tr>
                            <th>عدد المشاهدات</th>
                            <td>{{$news->visit}}</td>
                        </tr>
                    </table>
                    <div class="material-design-btn">
                    <a href="{{route('news.edit',$news)}}" class="btn notika-btn-bluegray btn-reco-mg btn-button-mg">تعديل</a>
                    <button data-toggle="modal" data-target="#myModalfive" class="btn notika-btn-red btn-reco-mg btn-button-mg">حذف</button>
                        <div class="modal animated flash" id="myModalfive" role="dialog">
                            <div class="modal-dialog modals-default nk-red">
                                <form action="{{route('news.destroy',$news)}}" method="post" >
                                    @method('DELETE')
                                    @csrf

                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <h2>حذف العنصر</h2>
                                        <p>من فضلك قم بتأكيد عملية الحذف..</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-default" style="display: inline-block">حذف</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
    </script>
@endpush
