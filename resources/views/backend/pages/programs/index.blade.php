@extends('backend.app')
@section('title') New News @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>برامج الجمعية</h2>
                                        <p>  جميع البرامج ({{\App\Program::count()}})</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="alert-area">
        <div class="container">
            @if(count($data) == 0 )
                <div class="row" >
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <p class="alert alert-danger text-center text-danger "> لم يتم حفظ أي بيانات بعد!!</p>
                    </div>
                </div>
            @endif
            @foreach($data as  $key=>$row)
            <div class="row rounded " style="padding: 20px; margin: 20px 0 ; background-color: white; text-align: right;">

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="direction: rtl">
                    <h4 >{{$row->title}}</h4>
                    <p>{{substrwords($row->content,200)}}</p>
                    <table class="table table-responsive table-bordered">
                        <tr class="bg-success">
                            <th>تاريخ الإنشاء</th>
                            <th>عدد المشاهدات</th>
                            <th>عدد قصص النجاح</th>
                            <th>عدد حالات المساعدة</th>
                       </tr>

                        <tr>
                            <td>{{$row->created_at->toDayDateTimeString()}}</td>
                            <td>{{$row->visit}}</td>
                            <td>{{$row->stories()->count()}}</td>
                            <td>{{$row->helps()->count()}}</td>
                        </tr>
                    </table>
                </div>


                <div class=" col-md-12 material-design-btn" style="margin-top: 30px;">
                    <a href="{{route('programs.show',$row)}}" class="btn notika-btn-green btn-reco-mg btn-button-mg">عرض</a>
                </div>

            </div>
            @endforeach
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center" >
                    {{$data->onEachSide(3)->links()}}
                </div>
            </div>
        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
    </script>
@endpush
