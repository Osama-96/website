@extends('backend.app')
@section('title') {{$item->title}} @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2> برامج الجمعية</h2>
                                        <p> {{$item->title}} </p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="alert-area">
        <div class="container">

            <div class="row rounded " style="padding: 20px; margin: 20px 0 ; background-color: white; text-align: right;">
                <div class="col-md-12" >
                    <h4 style="margin: 25px 0; text-align: center;" >{{$item->title}}</h4>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="direction: rtl; margin-top: 50px;">
                    <h4> فكرة البرنامج:</h4>
                    <div class=" mt-5" style="margin-bottom: 30px;">
                    {!! $item->content !!}
                    </div>
                </div>

                   <div class="row">
                       <div class="col-md-12 " style="border-top: #dddddd 1.5px solid; margin-top: 100px;padding-top: 100px; direction: rtl" >
                           <h4 style="text-align: center;margin-bottom: 50px;"> قصص النجاح</h4>
                           <h5>العنوان الفرعي لقصص النجاح:</h5>
                           <p> {{$item->story_sub_title}}</p>
                           <h5 style="margin-top: 50px;"> النص لقصص النجاح:</h5>
                           <p> {{$item->story_text}}</p>
                       </div>
                       @if($item->stories()->count())
                       <div class="col-md-12 table-responsive"style="direction: rtl">
                       <h4 style="margin-top: 50px;">القصص المحفوظة</h4>
                           <table class="table  table-bordered " >
                               <tr class="bg-success">
                                   <th> الفديو</th>
                                   <th> العنوان والمحتوى</th>
                                   <th style="max-width: 20%" class="text-center"> العمليات </th>
                               </tr>
                               @foreach($item->stories as $key => $story)
                               <tr>
                                   <td  style="width: 25%">
                                       <div class=" embed-responsive embed-responsive-16by9">
                                           <iframe class="embed-responsive-item" style="width: 100%; min-width:200px; height:100% " src=" https://www.youtube.com/embed/{{$story->link}}" allowfullscreen></iframe>
                                       </div>
                                   </td>
                                   <td>
                                       <h6>{{$story->title}}</h6>
                                       <p >{{$story->content}}</p>
                                   </td>

                                   <td class="material-design-btn text-center" style="width: 20%">
                                       <button data-toggle="modal" data-target="#myModalSE{{$key}}" style="margin-top: 10px ;" class="btn notika-btn-deeppurple btn-reco-mg btn-button-mg">تعديل</button>
                                       <div class="modal animated rubberBand" id="myModalSE{{$key}}" role="dialog">
                                           <div class="modal-dialog modal-large ">
                                               <form action="{{route('stories.update',$story)}}" method="post" >
                                                   @csrf
                                                   @method('PUT')
                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <button type="button" class="close" style="background-color: #5a4a9a" data-dismiss="modal">&times;</button>
                                                       </div>
                                                       <div class="modal-body card ">
                                                           <h2>تعديل قصة نجاح</h2>
                                                           <div class="form-group row text-right">
                                                               <div class="col-md-12">
                                                                   <label for="title">العنوان:</label>
                                                                   <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                       <input type="text" class="form-control" name="title" id="title" value="{{$story->title}}" reqired>
                                                                   </div>
                                                               </div>
                                                               <div class="col-md-12" style="margin: 25px 0;">
                                                                   <label for="content">الوصف:</label>
                                                                   <div class="">
                                                                       <textarea class="form-control" name="content" id="content" reqired> {{$story->content}}</textarea>
                                                                   </div>
                                                               </div>
                                                               <div class="col-md-12" style="margin: 25px 0;">
                                                                   <label for="link">لينك اليوتيوب:</label>
                                                                   <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                       <input type="text" class="form-control" name="link" id="link" value="https://www.youtube.com/watch?v={{$story->link}}" reqired>
                                                                   </div>
                                                               </div>
                                                           </div>


                                                       </div>
                                                       <div class="modal-footer">
                                                           <button type="submit" class="btn notika-btn-deeppurple btn-reco-mg btn-button-mg" style="display: inline-block">تحديث</button>
                                                       </div>
                                                   </div>
                                               </form>
                                           </div>
                                       </div>

                                       <button data-toggle="modal" data-target="#myModalSD{{$key}}" style="margin-top: 10px ;" class="btn notika-btn-red btn-reco-mg btn-button-mg">حذف</button>
                                       <div class="modal animated flash" id="myModalSD{{$key}}" role="dialog">
                                           <div class="modal-dialog modals-default nk-red">
                                               <form action="{{route('stories.destroy',$story)}}" method="post" >
                                                   @method('DELETE')
                                                   @csrf

                                                   <div class="modal-content">
                                                       <div class="modal-header">
                                                           <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                       </div>
                                                       <div class="modal-body">
                                                           <h2>حذف العنصر</h2>
                                                           <p>من فضلك قم بتأكيد عملية الحذف..</p>
                                                       </div>
                                                       <div class="modal-footer">
                                                           <button type="submit" class="btn btn-default" style="display: inline-block">حذف</button>
                                                       </div>
                                                   </div>
                                               </form>
                                           </div>
                                       </div>
                                   </td>
                               </tr>
                               @endforeach
                           </table>
                       </div>
                       @endif

                   </div>


                    <div class="row">
                        <div class="col-md-12" style="border-top: #dddddd 1.5px solid; margin-top: 100px;padding-top: 100px; direction: rtl">
                            <h4 style="text-align: center;margin-bottom: 50px;"> تقدر تساعد</h4>
                            <h5>العنوان الفرعي لتقدر تساعد:</h5>
                            <p> {{$item->can_help_sub_title}}</p>
                        </div>
                        @if ($item->helps()->count())
                        <div class="col-md-12" style="margin-top: 50px; direction: rtl">
                            <h3>الحالات المحفوظة</h3>
                            <table class="table table-bordered ">
                                <tr class="bg-info">
                                    <th>الصورة</th>
                                    <th>النص</th>
                                    <th>التاريخ</th>
                                    <th style="max-width: 20%" class="text-center">العمليات</th>
                                </tr>
                                @foreach($item->helps as $key => $help)
                                <tr>
                                    <td>
                                        <img src="{{asset($help->file)}}" width="250px" alt="الصورة غير متوفرة">
                                    </td>
                                    <td>
                                        {{$help->text}}
                                    </td>
                                    <td style="width: 20%">
                                        {{$help->created_at->toDayDateTimeString()}}
                                    </td>
                                    <td class="material-design-btn text-center" style="width: 20%">
                                        <button data-toggle="modal" data-target="#myModalSh{{$key}}" style="margin-top: 10px ;" class="btn notika-btn-deeppurple btn-reco-mg btn-button-mg">تعديل</button>
                                        <div class="modal animated rubberBand" id="myModalSh{{$key}}" role="dialog">
                                            <div class="modal-dialog modal-large ">
                                                <form action="{{route('helps.update',$help)}}" method="post" enctype="multipart/form-data" >
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" style="background-color: #5a4a9a" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body card ">
                                                            <h2>تعديل حالة مساعدة</h2>
                                                            <div class="form-group row text-right">

                                                                <div class="col-md-12" style="margin: 25px 0;">
                                                                    <label for="content">النص:</label>
                                                                    <div class="">
                                                                        <textarea class="form-control" name="text" id="content" rows="7" reqired> {{$help->text}}</textarea>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-12" style="margin: 25px 0;">
                                                                    <label for="link">الصورة</label>
                                                                    <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                        <img class="rounded" width="400" height=""   id="imgPreviewe" src="{{asset($help->file)}}" alt="الصورة غير متوفرة" >
                                                                        <input type="file" class="form-control-file" id="file" name='file'  onchange="readURL(this,'#imgPreviewe')" />
                                                                        @error('file') <small>{{$message}}</small> @enderror
                                                                    </div>
                                                                </div>
                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn notika-btn-deeppurple btn-reco-mg btn-button-mg" style="display: inline-block">تحديث</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <button data-toggle="modal" data-target="#myModalShd{{$key}}" style="margin-top: 10px ;" class="btn notika-btn-red btn-reco-mg btn-button-mg">حذف</button>
                                        <div class="modal animated flash" id="myModalShd{{$key}}" role="dialog">
                                            <div class="modal-dialog modals-default nk-red">
                                                <form action="{{route('helps.destroy',$help)}}" method="post" >
                                                    @method('DELETE')
                                                    @csrf

                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h2>حذف العنصر</h2>
                                                            <p>من فضلك قم بتأكيد عملية الحذف..</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-default" style="display: inline-block">حذف</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach

                            </table>

                        </div>
                        @endif
                    </div>
                    <div class="row" style="direction: rtl; margin-top: 100PX;">
                        <div class="col-md-12">
                                <h4 class="text-center">بيانات أخرى </h4>
                            <table class="table table-responsive table-bordered">
                                <tr class="bg-success">
                                    <th>تاريخ الإنشاء</th>
                                    <th>عدد المشاهدات</th>
                                    <th>عدد قصص النجاح</th>
                                    <th>عدد حالات المساعدة</th>
                                </tr>

                                <tr>
                                    <td>{{$item->created_at->toDayDateTimeString()}}</td>
                                    <td>{{$item->visit}}</td>
                                    <td>{{$item->stories()->count()}}</td>
                                    <td>{{$item->helps()->count()}}</td>
                                </tr>
                            </table>
                            <div class="material-design-btn text-center" >
                                <a href="{{route('programs.edit',$item)}}" class="btn notika-btn-bluegray btn-reco-mg btn-button-mg">تعديل</a>
                                <button data-toggle="modal" data-target="#myModalThree" class="btn notika-btn-green btn-reco-mg btn-button-mg">إضافة قصة نجاح</button>
                                <div class="modal animated rubberBand" id="myModalThree" role="dialog">
                                    <div class="modal-dialog modal-large ">
                                        <form action="{{route('stories.store',$item)}}" method="post" >
                                            @csrf

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" style="background-color: #3ba830" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body card ">
                                                    <h2>إضافة قصة نجاح</h2>
                                                    <div class="form-group row text-right">
                                                        <div class="col-md-12">
                                                            <label for="title">العنوان:</label>
                                                            <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                <input type="text" class="form-control" name="title" id="title" reqired>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12" style="margin: 25px 0;">
                                                            <label for="content">الوصف:</label>
                                                            <div class="">
                                                                <textarea class="form-control" name="content" id="content" reqired> </textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12" style="margin: 25px 0;">
                                                            <label for="link">لينك اليوتيوب:</label>
                                                            <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                <input type="text" class="form-control" name="link" id="link" reqired>
                                                            </div>
                                                        </div>


                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn notika-btn-green btn-reco-mg btn-button-mg" style="display: inline-block">حفظ</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <button data-toggle="modal" data-target="#myModalFour"  class="btn notika-btn-orange btn-reco-mg btn-button-mg">إضافة حالة للمساعدة</button>
                                <div class="modal animated rubberBand" id="myModalFour" role="dialog">
                                    <div class="modal-dialog modal-large ">
                                        <form action="{{route('helps.store',$item)}}" method="post" enctype="multipart/form-data">
                                            @csrf

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" style="background-color: #da9c17" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body card ">
                                                    <h2>إضافة حالة للمساعدة</h2>
                                                    <div class="form-group row text-right">

                                                        <div class="col-md-12" style="margin: 25px 0;">
                                                            <label for="content">النص:</label>
                                                            <div class="">
                                                                <textarea class="form-control" name="text" id="content" rows="7"> </textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12" style="margin: 25px 0;">
                                                            <label for="link"> الصورة:</label>
                                                            <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                <img class="rounded" width="400" height=""   id="imgPreview" src="" alt="" >
                                                                <input type="file" class="form-control-file" id="file" name='file'  onchange="readURL(this,'#imgPreview')" required/>
                                                                @error('file') <small>{{$message}}</small> @enderror
                                                            </div>
                                                        </div>


                                                    </div>


                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn notika-btn-orange btn-reco-mg btn-button-mg" style="display: inline-block">حفظ</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <button data-toggle="modal" data-target="#myModalfive" class="btn notika-btn-red btn-reco-mg btn-button-mg">حذف</button>
                                <div class="modal animated flash" id="myModalfive" role="dialog">
                                    <div class="modal-dialog modals-default nk-red">
                                        <form action="{{route('programs.destroy',$item)}}" method="post" >
                                            @method('DELETE')
                                            @csrf

                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                </div>
                                                <div class="modal-body">
                                                    <h2>حذف العنصر</h2>
                                                    <p>من فضلك قم بتأكيد عملية الحذف..</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" style="display: inline-block">حذف</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                </div>

            </div>

        </div>

    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
        $('body').on('click','.frame',function() {
            video = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" style="height:100% " src="' + $(this).attr('data-video') + '" allowfullscreen></div>';
            $(this).replaceWith(video);
        });
    </script>
@endpush
