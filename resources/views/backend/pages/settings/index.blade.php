@extends('backend.app')
@section('title') Settings @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>الإعدادات العامة</h2>
                                        <p></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="invoice-area ">
        <div class="container">

            <div class="row" style="text-align: right; direction: rtl" >

                    <div class="col-lg-12 col-md-12 " >
                        <form action="{{route('settings.store')}}" method="post">
                            @csrf
                        <div class="invoice-wrap">
                                <div class="form-group">
                                    <h4 style="color: #0B792F; margin-bottom: 40px;">العدادات</h4>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span>اسم العداد</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="counter1_name" value="{{$counter1_name??''}}" required>
                                        </div>
                                        <div class="col-md-1">
                                            <span>العدد</span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" min="1" class="form-control" name="counter1_value"  value="{{$counter1_value??''}}" required>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span>اسم العداد</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="counter2_name" value="{{$counter2_name??''}}" required>
                                        </div>
                                        <div class="col-md-1">
                                            <span>العدد</span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" min="1" class="form-control" name="counter2_value" value="{{$counter2_value??''}}" required>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span>اسم العداد</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="counter3_name" value="{{$counter3_name??''}}" required>
                                        </div>
                                        <div class="col-md-1">
                                            <span>العدد</span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" min="1" class="form-control" name="counter3_value" value="{{$counter3_value??''}}" required>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span>اسم العداد</span>
                                        </div>
                                        <div class="col-md-6">
                                            <input type="text" class="form-control" name="counter4_name" value="{{$counter4_name??''}}" required>
                                        </div>
                                        <div class="col-md-1">
                                            <span>العدد</span>
                                        </div>
                                        <div class="col-md-2">
                                            <input type="number" min="1" class="form-control" name="counter4_value" value="{{$counter4_value??''}}" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="invoice-wrap">
                                <div class="form-group">
                                    <h4 style="color: #0B792F; margin-bottom: 40px;">وسائل التبرع</h4>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span>رقم أورانج</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="orange_number" value="{{$orange_number??''}}" required >
                                        </div>
                                        <div class="col-md-2">
                                            <span>رقم فودافون</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="vodafoon_number"value="{{$vodafoon_number??''}}" required >
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span> حساب البنك</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="bank_account"value="{{$bank_account??''}}"  required>
                                        </div>
                                        <div class="col-md-2">
                                            <span>كود الحساب </span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="account_code"value="{{$account_code??''}}"  required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="invoice-wrap">
                                <div class="form-group">
                                    <h4 style="color: #0B792F; margin-bottom: 40px;">إعدادت أخرى</h4>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span>لينك الفيسبوك</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="fb_link" value="{{$fb_link??''}}" required>
                                        </div>
                                        <div class="col-md-2">
                                            <span>لينك  الانستجرام </span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="insta_link" value="{{$insta_link??''}}" required>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                    <div class="col-md-2">
                                            <span>لينك  اليوتيوب </span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="youtube_link"value="{{$youtube_link??''}}" required >
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">
                                        <div class="col-md-2">
                                            <span>الهاتف الأول</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="phone1"value="{{$phone1??''}}" required >
                                        </div>
                                        <div class="col-md-2">
                                            <span>الهاتف الثاني </span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="text" class="form-control" name="phone2"value="{{$phone2??''}}"  required>
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 20px;">

                                    <div class="col-md-2">
                                            <span>إيميل الجمعية</span>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="email" class="form-control" name="email" value="{{$email}}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="invoice-wrap">
                                <div class="form-group">
                                    <div class="vw-ml-action-ls text-right mg-t-20" style="text-align: center" >
                                        <div class="btn-group ib-btn-gp active-hook nk-email-inbox text-center" >
                                            <button class="btn btn-default btn-sm waves-effect"><i class="notika-icon notika-checked"></i> تحديث</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>

            </div>

        </div>
    </div>
    <!-- Invoice area End-->
@stop

