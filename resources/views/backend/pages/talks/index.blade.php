@extends('backend.app')
@section('title') Talks @stop
@section('content')
    <!-- Breadcomb area Start-->
    <div class="breadcomb-area" >
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcomb-list">
                        <div class="row" dir="rtl">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="breadcomb-wp">
                                    <div class="breadcomb-icon">
                                        <i class="notika-icon notika-support"></i>
                                    </div>
                                    <div class="breadcomb-ctn" style="margin-right: 20px;">
                                        <h2>قالوا عنا</h2>
                                        <p> جميع المقالات</p>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Breadcomb area End-->
    <!-- Invoice area Start-->
    <div class="alert-area">
        <div class="container">
            @if(count($data) == 0 )
                <div class="row" >
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" >
                        <p class="alert alert-danger text-center text-danger "> لم يتم حفظ أي بيانات بعد!!</p>
                    </div>
                </div>
            @else

            <div class="row rounded " style="padding: 20px; margin: 20px 0 ; background-color: white; text-align: right;">

                <div class="col-lg-12 col-md-8 col-sm-12 col-xs-12 table-responsive" style="direction: rtl">
                    <table class="table table-bordered ">
                        <tr>
                            <th>#</th>
                            <th>الصورة</th>
                            <th>بيانات القائل</th>
                            <th>النص</th>
                            <th>العمليات</th>
                        </tr>
                        @foreach($data as  $key=>$row)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td style="width: 15%">
                                    <img src="{{asset($row->file)}}" style="width: 100%" class="img-responsive  img-circle " alt="">
                                </td>
                                <td style="width: 15%">
                                    <h5>{{$row->name??'لا يوجد'}}</h5>
                                    <h6>{{$row->job??'لا يوجد'}}</h6>

                                </td>
                                <td>{{$row->text}}</td>
                                <td class="material-design-btn text-center" style="width: 20%">

                                        <button data-toggle="modal" data-target="#myModalSE{{$key}}" style="margin-top: 10px ;" class="btn notika-btn-deeppurple btn-reco-mg btn-button-mg">تعديل</button>
                                        <div class="modal animated rubberBand" id="myModalSE{{$key}}" role="dialog">
                                            <div class="modal-dialog modal-large ">
                                                <form action="{{route('talks.update',$row)}}" method="post"  enctype="multipart/form-data">
                                                    @csrf
                                                    @method('PUT')
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" style="background-color: #5a4a9a" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body card ">
                                                            <h2>تعديل مقولة</h2>
                                                            <div class="row">
                                                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                                    <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                                        <span>سم القائل: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="cmp-int-box mg-t-20">
                                                                            <input type="text" name="name" class="form-control" placeholder="" value="{{$row->name}}"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                                    <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                                        <span>الوظيفة: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="cmp-int-box mg-t-20">
                                                                            <input type="text" name="job" class="form-control" placeholder="" value="{{$row->job}}"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                                    <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                                        <span>صورة القائل: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="nk-int-st cmp-int-in cmp-email-over">
                                                                            <input type="file" name="file" class="form-control"  onchange="readURL(this,'#imgPreview1{{$key}}')"  />
                                                                        </div>
                                                                        <img src="{{$row->file}}" alt="" width="450" class="img-responsive" id="imgPreview1{{$key}}" >
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-1 col-md-2 col-sm-2 col-xs-12">
                                                                    <div class="cmp-int-lb cmp-int-lb1 text-right">
                                                                        <span>النص: </span>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-11 col-md-10 col-sm-10 col-xs-12">
                                                                    <div class="form-group">
                                                                        <div class="cmp-int-box mg-t-20">
                                                                            <textarea class="form-control" maxlength="400"  name="text">{{$row->text}}</textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>


                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn notika-btn-deeppurple btn-reco-mg btn-button-mg" style="display: inline-block">تحديث</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>

                                        <button data-toggle="modal" data-target="#myModalSD{{$key}}" style="margin-top: 10px ;" class="btn notika-btn-red btn-reco-mg btn-button-mg">حذف</button>
                                        <div class="modal animated flash" id="myModalSD{{$key}}" role="dialog">
                                            <div class="modal-dialog modals-default nk-red">
                                                <form action="{{route('talks.destroy',$row)}}" method="post" >
                                                    @method('DELETE')
                                                    @csrf

                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <h2>حذف العنصر</h2>
                                                            <p>من فضلك قم بتأكيد عملية الحذف..</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-default" style="display: inline-block">حذف</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                </td>
                            </tr>
                        @endforeach

                    </table>

                </div>


            </div>
            @endif

        </div>
    </div>
    <!-- Invoice area End-->
@stop
@push('js')
    <script !src="">
        $(document).ready(function() {
            $('#summernote').summernote({
                tabsize: 2,
                height: 350,
            });
        });
    </script>
@endpush
