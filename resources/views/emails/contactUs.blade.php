@component('mail::message')
<div class="container" style="direction: rtl ">

    <div class="row">
        <div class="col-md-12 text-center">
            <h3>جمعية مصر المحروسة بلدي</h3>
            <h3>موقع الجمعية </h3>
        </div>

    </div>


    <div class="row justify-content-center">
         <div class="col-md-10 text-right">
             <table class="table">
                 <tr>
                     <th>المرسل:</th>
                     <td>{{$data['name']}}</td>
                 </tr>
                 <tr>
                     <th>الهاتف:</th>
                     <td>
                         <a href="tel:{{$data['phone']}}" class="btn btn-link">{{$data['phone']}}</a>
                     </td>
                 </tr>
                 <tr>
                     <th>الغرض:</th>
                     <td>
                         {{$data['reason']}}
                     </td>
                 </tr>
             </table>
             <h5> الرسالة</h5>
             <p>
                 {{$data['message']}}
             </p>
         </div>
    </div>
</div>


شكراً<br>
{{ config('app.name') }}
@endcomponent
