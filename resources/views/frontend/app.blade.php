<!DOCTYPE html>
<html lang="en">
<head><title> Masr Elmahrusa | @yield('title')</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/images/logo.png')}}">

    <!-- LIBRARY FONT-->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,400italic,700,900,300">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/font/font-icon/font-awesome-4.4.0/css/font-awesome.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/font/font-icon/font-svg/css/Glyphter.css')}}">
    <!-- LIBRARY CSS-->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/libs/animate/animate.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/libs/bootstrap-3.3.5/css/bootstrap.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/libs/owl-carousel-2.0/assets/owl.carousel.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/libs/selectbox/css/jquery.selectbox.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/libs/fancybox/css/jquery.fancybox.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/libs/fancybox/css/jquery.fancybox-buttons.css')}}">
    <link type="text/css" rel="stylesheet" href="{{asset('assets/libs/media-element/build/mediaelementplayer.min.css')}}">
    <!-- STYLE CSS    -->
    <link type="text/css" rel="stylesheet" href="{{asset('assets/css/color-1-rtl.css')}}" id="color-skins">
    <!--link(type="text/css", rel='stylesheet', href='#', id="color-skins")-->
    <script src="{{asset('assets/libs/jquery/jquery-2.1.4.min.js')}}"></script>
    <style>
        @font-face {
            font-family: kufi;
            src: url({{asset('fonts/ArbFONTS-Droid.Arabic.Kufi_DownloadSoftware.iR_.ttf')}});
        }
        *{
            font-family: kufi;
        }
        .blockquote{
            quotes: "\201D""\201C""\2018""\2019";
            line-height: 30px;
            direction: rtl;
            padding-left: 3em;
            padding-right: 3em;
            position: relative;

        }
        .blockquote:before{
            content: open-quote "\00a0";
            font-weight: bold;
            font-size:40px;
            color: #2e9c74;
            margin: 20px  ;
            position: absolute;
            top: -10px;
            right: 0;
        }
        .blockquote:after{
            content: close-quote;
            font-weight: bold;
            font-size:40px;
            color:#2e9c74;
            margin:  20px  ;
            bottom: -25px;
            left: 0;


        }
        .nav-tabs > li {
            float: right;
        }
        @media (min-width: 768px){
            .navbar-nav > li {
                float: right;
            }
        }


    </style>
@yield('css')
@stack('css')
    <!--script.--><!--    if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1')) {--><!--        $('#color-skins').attr('href', 'assets/css/' + Cookies.get('color-skin') + '.css');--><!--    } else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1')) {--><!--        $('#color-skins').attr('href', 'assets/css/color-1.css');--><!--    }--></head>
<body><!-- HEADER-->
@include('frontend.includes.header')
@if (session()->has('sent'))
    <div class="alert alert-success text-center" style="margin: 0 0 ;">{{session()->get('sent')}}</div>
@endif
@if (session()->has('notSent'))
    <div class="alert alert-danger text-center" style="margin: 0 0 ;">{{session()->get('notSent')}}</div>
@endif
@yield('content')
@include('frontend.includes.footer')
<!-- LOADING-->
<div class="body-2 loading">
    <div class="dots-loader"></div>
</div>
<!-- JAVASCRIPT LIBS--><!--script.--><!--    if ((Cookies.get('color-skin') != undefined) && (Cookies.get('color-skin') != 'color-1')) {--><!--        $('.logo .header-logo img').attr('src', 'assets/images/logo-' + Cookies.get('color-skin') + '.png');--><!--    } else if ((Cookies.get('color-skin') == undefined) || (Cookies.get('color-skin') == 'color-1')) {--><!--        $('.logo .header-logo img').attr('src', 'assets/images/logo-color-1.png');--><!--    }-->
<script src="{{asset('assets/libs/bootstrap-3.3.5/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/libs/owl-carousel-2.0/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/libs/appear/jquery.appear.js')}}"></script>
<script src="{{asset('assets/libs/count-to/jquery.countTo.js')}}"></script>
<script src="{{asset('assets/libs/wow-js/wow.min.js')}}"></script>
<script src="{{asset('assets/libs/selectbox/js/jquery.selectbox-0.2.min.js')}}"></script>
<script src="{{asset('assets/libs/fancybox/js/jquery.fancybox.js')}}"></script>
<script src="{{asset('assets/libs/fancybox/js/jquery.fancybox-buttons.js')}}"></script>
<!-- MAIN JS-->
<script src="{{asset('assets/js/main.js')}}"></script>
<!-- LOADING SCRIPTS FOR PAGE-->
<script src="{{asset('assets/libs/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/libs/isotope/fit-columns.js')}}"></script>
<script src="{{asset('assets/js/pages/homepage.js')}}"></script>
@yield('js')
@stack('js')

</body>
</html>

