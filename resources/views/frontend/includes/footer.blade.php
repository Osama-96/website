<footer dir="rtl">
    <div class="footer-top">
        <div class="container"  >
            <div class="footer-top-wrapper " >
                <div class="footer-top-left " style="padding-top:20px; ">
                    <p class="footer-top-focus">تقدر تساعد..</p>

                    <p class="footer-top-text">يمكنك كفالة أسرة فقيرة أو طفل يتيم او مساعدة أب في كفالة أبنائه، فقط أدخل بياناتك وسنقوم بالتواصل معك...</p>
                    <div  class="row  " style="padding: 50px 0; ">
                        <div class="col-md-4 text-center p-2">
                            <div style="text-align: center ; font-size: 62px;">

                                <img src="{{asset('assets/images/bnk-msr.png')}}" style="width: 150px;"  alt="" >
                                <p style="color: #ffffff; font-size: 18px"><span style="color: #0f0f0f">حساب رقم:</span>

                                    {{\DB::table('settings')->where('key','bank_account')->select('value')->first()->value}}
                                    <br>
                                    <span style="color: #0f0f0f">كود:</span>
                                    {{\DB::table('settings')->where('key','account_code')->select('value')->first()->value}}
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="text-align: center ; font-size: 62px;">
                                <img src="{{asset('assets/images/vodafon.png')}}" style="width: 150px;"  alt="" >
                                <p style="color: #ffffff; font-size: 18px">{{\DB::table('settings')->where('key','vodafoon_number')->select('value')->first()->value}}</p>
                            </div>

                        </div>
                        <div class="col-md-4">
                            <div style="text-align: center ; font-size: 62px;">
                                <img src="{{asset('assets/images/orange.png')}}" style="width: 150px;"  alt="" >
                                <p style="color: #ffffff; font-size: 18px">{{\DB::table('settings')->where('key','orange_number')->select('value')->first()->value}}</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" justify-content-center text-center"  style="margin-top:10%; ">
                    <form action="{{route('mails.store')}}" method="post">
                        @csrf
                        <div style="text-align: right; padding-right: 20px;">
                        <h3>تواصل معنا..</h3>
                        <p >يمكنك أن تترك بياناتك وسنقوم بالتواصل معك، وإرسال مندوبنا إلى باب باب منزلك..</p>
                        </div>
                        <div style="text-align: center; padding-right: 20px;">
                            <div class="d-block" style=" padding: 0 0px ;">
                                <input type="text"  placeholder="الاسم" class="form-control mx-2" name="name" id=""  required>
                            </div>
                            <div class="d-block" style=" padding: 5px 0px ;">
                                <input type="text" placeholder="الهاتف" class="form-control" name="phone" id="" required>
                                <input  type="text" value="من أجل التبرع"  class="form-control" name="reason" id="" required hidden="hidden" style="display: none"/>
                            </div>
                            <div class="d-block" style=" padding: 5px 0px ;">
                                <textarea name="message" placeholder="اترك رسالتك.." class="form-control" id="" rows="5" required></textarea>
                            </div>
                        </div>

                        <div class="text-left">

                        <button type="submit" class="btn btn-blue btn-bold " style="margin-bottom: 30px;"><span> اطلب التواصل</span></button>
                        </div>

                    </form>
                </div>


            </div>
        </div>
    </div>
    <div class="footer-main" >
        <div class="container">
            <div class="footer-main-wrapper" >
                <div class="row">
                    <div class="col-2"  >
                        <div class="col-md-4 col-sm-6 col-xs-6 sd380" style="float: right">
                            <div class="edugate-widget widget">
                                <div class="title-widget">جمعية مصر المحروسة بلدي</div>
                                <div class="content-widget">
                                    <p>جمعية مصر المحروسة بلدي جمعية أهلية تنموية مصرية مركزية  تابعة لوزارة التضامن الإجتماعي تأسست عام 2000 لخدمة المجتمع عن طريق بناء الإنسان  .</p>
                                    <div class="info-list">
                                        <ul class="list-unstyled">
                                            <li><i class="fa fa-envelope-o"></i><a href="mailto:{{\DB::table('settings')->where('key','phone1')->select('value')->first()->value}}">{{\DB::table('settings')->where('key','email')->select('value')->first()->value}}</a></li>
                                            <li><i class="fa fa-phone"></i><a href="tel: +2{{\DB::table('settings')->where('key','phone1')->select('value')->first()->value}}">+2{{\DB::table('settings')->where('key','phone1')->select('value')->first()->value}}</a></li>
                                            <li><i class="fa fa-map-marker"></i>
                                                <a href="#">
                                                    <p>أرض الخدمات بالمعصرة  - بجوار مجمع المدارس – طريق الأوتوستراد </p>
                                                </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-6 sd380" style="float: right">
                            <div class="useful-link-widget widget">
                                <div class="title-widget">روابط سريعة</div>
                                <div class="content-widget">
                                    <div class="useful-link-list">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <ul class="list-unstyled">
                                                    <li><i class="fa fa-angle-left"></i><a href="{{route('about')}}">من نحن</a></li>
                                                    <li><i class="fa fa-angle-left"></i><a href="{{route('newNews')}}">أخبار الجمعية</a></li>
                                                    <li><i class="fa fa-angle-left"></i><a href="{{route('site_activities')}}">أنشطة الجمعية</a></li>
                                                    <li><i class="fa fa-angle-left"></i><a href="{{route('faq')}}">الأسئلة الشائعة</a></li>
                                                    <li><i class="fa fa-angle-left"></i><a href="{{route('contact')}}">تواصل معنا</a></li>
                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="col-md-5 col-sm-12 col-xs-12 sd380" style="float: right">
                            <div class="gallery-widget widget">
                                <div class="title-widget">أنشطة الجمعية</div>
                                <div class="content-widget">
                                    <div class="gallery-list">
                                        @foreach(\App\Activity::whereNotNull('images')->inRandomOrder()->select('images','slug')->limit(8)->get() as $key=>$item)
                                        <a href="{{route('site_activities_details',$item->slug)}}" class="thumb"><img src="{{asset($item->images[0])}}" style="width: 200px; " alt="" class="img-responsive"/></a>
                                        @endforeach
                                        <div class="clearfix"></div>
                                    <a href="{{route('site_activities')}}" class="view-more">مشاهدة الكل&nbsp;<i class="fa fa-angle-double-left mls"></i></a></div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</footer>
