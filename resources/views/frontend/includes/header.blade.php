<header>
    <div class="header-topbar">
        <div class="container">
            <div class="topbar-left pull-left">
                <div class="email"><a href="mailto:{{ \DB::table('settings')->where('key','email')->select('value')->first()->value}}"><i class="topbar-icon fa fa-envelope-o"></i><span>{{\DB::table('settings')->where('key','email')->select('value')->first()->value}}</span></a></div>
                <div class="hotline"><a href="tel: +2{{\DB::table('settings')->where('key','phone1')->select('value')->first()->value}}"><i class="topbar-icon fa fa-phone"></i><span>+2{{\DB::table('settings')->where('key','phone1')->select('value')->first()->value}}</span></a></div>
            </div>
            <div class="topbar-right pull-right">
                <div class="socials">
                    <a href="{{\DB::table('settings')->where('key','fb_link')->select('value')->first()->value}}" class="facebook"><i class="fa fa-facebook"></i></a>
                    <a href="{{\DB::table('settings')->where('key','insta_link')->select('value')->first()->value}}" class="instagram"><i class="fa fa-instagram"></i></a>
                    <a href="{{\DB::table('settings')->where('key','youtube_link')->select('value')->first()->value}}" class="youtube"><i class="fa fa-youtube"></i></a>
{{--                    <a href="#" class="blog"><i class="fa fa-rss"></i></a>--}}
{{--                    <a href="#" class="dribbble"><i class="fa fa-dribbble"></i></a></div>--}}
{{--                <div class="group-sign-in"><a href="login.html" class="login">login</a><a href="register.html" class="register">register</a></div>--}}
            </div>
        </div>
    </div>
    <div class="header-main homepage-01">
        <div class="container">
            <div class="header-main-wrapper">
                <div class="navbar-heade">
                    <div class="logo pull-left"><a href="{{route('/')}}" class="header-logo"><img src="assets/images/logo.png"   alt=""/></a></div>
                    <button type="button" data-toggle="collapse" data-target=".navigation" class="navbar-toggle edugate-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                </div>
                <nav class="navigation collapse navbar-collapse pull-right">
                    <ul class="nav-links nav navbar-nav">
                        <li><a href="{{route('/')}}" class="main-menu">الرئيسية</a></li>
                        <li><a href="{{route('about')}}" class="main-menu">من نحن</a></li>
                        <li><a href="{{route('Site_News_Page')}}" class="main-menu">أخبار الجمعية</a></li>

                        @if(\App\Program::count())
                        <li  class="dropdown"><a href="javascript:void(0)" class="main-menu">برامج الجمعية<span class="fa fa-angle-down icons-dropdown"></span></a>
                            <ul class="dropdown-menu edugate-dropdown-menu-1">
                                @foreach(DB::table('programs')->whereNull('deleted_at')->select('title','slug')->get() as $key=>$item)
                                <li><a href="{{route('program',$item->slug)}}" class="link-page">{{$item->title}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        @endif

                        <li><a href="{{route('site_activities')}}" class="main-menu">أنشطة الجمعية</a></li>
                        <li><a href="{{route('faq')}}" class="main-menu"> الأسئلة الشائعة</a></li>
                        <li><a href="{{route('contact')}}" class="main-menu">تواصل معنا</a></li>

                    </ul>
                </nav>
            </div>
        </div>
    </div>
</header>
