<div class="section progress-bars section-padding" >
    <div class="container" >
        <div class="progress-bars-content">
            <div class="progress-bar-wrapper"><h2 class="title-2">إنجازات الجمعية</h2>

                <div class="row "   >
                    <div class="content" >
                        <div class="col-sm-3">
                            <div class="progress-bar-number">
                                <div data-from="0" data-to="{{\DB::table('settings')->where('key','counter1_value')->select('value')->first()->value}}" data-speed="1000" class="num">0</div>
                                <p class="name-inner">{{\DB::table('settings')->where('key','counter1_name')->select('value')->first()->value}}</p></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="progress-bar-number">
                                <div data-from="0" data-to="{{\DB::table('settings')->where('key','counter2_value')->select('value')->first()->value}}" data-speed="1000" class="num">0</div>
                                <p class="name-inner">{{\DB::table('settings')->where('key','counter2_name')->select('value')->first()->value}}</p></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="progress-bar-number">
                                <div data-from="0" data-to="{{\DB::table('settings')->where('key','counter3_value')->select('value')->first()->value}}" data-speed="1000" class="num">0</div>
                                <p class="name-inner">{{\DB::table('settings')->where('key','counter3_name')->select('value')->first()->value}}</p></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="progress-bar-number">
                                <div data-from="0" data-to="{{\DB::table('settings')->where('key','counter4_value')->select('value')->first()->value}}" data-speed="1000" class="num">0</div>
                                <p class="name-inner">{{\DB::table('settings')->where('key','counter4_name')->select('value')->first()->value}}</p></div>
                        </div>

                    </div>
                </div>
                {{--                                    <div class="group-button">--}}
                {{--                                        <button onclick="window.location.href='categories.html'" class="btn btn-transition-3"><span>Purchase theme</span></button>--}}
                {{--                                        <button onclick="window.location.href='about-us.html'" class="btn btn-green-3"><span>start Learn now</span></button>--}}
                {{--                                    </div>--}}
                <div class="group-btn-slider">
                    <div class="btn-prev"><i class="fa fa-angle-left"></i></div>
                    <div class="btn-next"><i class="fa fa-angle-right"></i></div>
                </div>
            </div>
        </div>
    </div>
</div>
