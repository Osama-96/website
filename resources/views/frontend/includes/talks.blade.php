

<div class="container">

    <div id="people-talk" data-ride="carousel" data-interval="5000" class="slider-talk-about-us-wrapper carousel slide">
        <div role="listbox" class="slider-talk-about-us-content carousel-inner ">
            <div class="peopel-item item  active">
                <p class="peopel-comment ">
                    هناك حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها. ولذلك يتم استخدام طريقة لوريم إيبسوم لأنها تعطي توزيعاَ طبيعياَ -إلى حد ما- للأحرف عوضاً عن استخدام "هنا يوجد محتوى نصي، هنا يوجد محتوى نصي" فتجعلها تبدو (أي الأحرف) وكأنها نص مقروء. العديد من برامح النشر المكتبي وبرامح تحرير صفحات على مدى السنين ظهرت نسخ جديدة ومختلفة من نص لوريم إيبسوم، إدخال بعض العبارات الفكاهية إليها.
                </p>

                <div class="group-peole-info">
                    <div class="peopel-avatar"><img src="assets/images/people-avatar-1.jpg" alt="" class="img-responsive"/></div>
                    <div class="peopel-name">john doe</div>
                    <div class="people-job">Microshop Crop.SEO</div>
                </div>
            </div>
            <div class="peopel-item item"><p class="peopel-comment">" There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. "</p>

                <div class="group-peole-info">
                    <div class="peopel-avatar"><img src="assets/images/people-avatar-1.jpg" alt="" class="img-responsive"/></div>
                    <div class="peopel-name">john doe</div>
                    <div class="people-job">Microshop Crop.SEO</div>
                </div>
            </div>
            <div class="peopel-item item"><p class="peopel-comment">" There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. "</p>

                <div class="group-peole-info">
                    <div class="peopel-avatar"><img src="assets/images/people-avatar-1.jpg" alt="" class="img-responsive"/></div>
                    <div class="peopel-name">john doe</div>
                    <div class="people-job">Microshop Crop.SEO</div>
                </div>
            </div>
        </div>
    </div>
</div>
