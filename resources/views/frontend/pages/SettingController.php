<?php

namespace App\Http\Controllers;

use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counter1_name = null;      $counter2_name = null;      $counter3_name = null;     $counter4_name = null;
        $counter1_value = null;     $counter2_value = null;     $counter3_value = null;    $counter4_value = null;
        $orange_number = null;      $vodafoon_number = null ;   $bank_account = null;      $account_code = null;
        $fb_link = null;            $insta_link = null;         $youtube_link = null ;      $phone1 = null;
        $phone2 = null ;            $email = null;

        if(\DB::table('settings')->where('key','counter1_name')->count()){
            $counter1_name = \DB::table('settings')->where('key','counter1_name')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','counter1_value')->count()){
            $counter1_value = \DB::table('settings')->where('key','counter1_value')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','counter2_name')->count()){
            $counter2_name = \DB::table('settings')->where('key','counter2_name')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','counter2_value')->count()){
            $counter2_value = \DB::table('settings')->where('key','counter2_value')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','counter3_name')->count()){
            $counter3_name = \DB::table('settings')->where('key','counter3_name')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','counter3_value')->count()){
            $counter3_value = \DB::table('settings')->where('key','counter3_value')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','counter4_name')->count()){
            $counter4_name = \DB::table('settings')->where('key','counter4_name')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','counter4_value')->count()){
            $counter4_value = \DB::table('settings')->where('key','counter4_value')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','orange_number')->count()){
            $orange_number = \DB::table('settings')->where('key','orange_number')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','vodafoon_number')->count()){
            $vodafoon_number = \DB::table('settings')->where('key','vodafoon_number')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','bank_account')->count()){
            $bank_account = \DB::table('settings')->where('key','bank_account')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','account_code')->count()){
            $account_code = \DB::table('settings')->where('key','account_code')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','fb_link')->count()){
            $fb_link = \DB::table('settings')->where('key','fb_link')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','insta_link')->count()){
            $insta_link = \DB::table('settings')->where('key','insta_link')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','youtube_link')->count()){
            $youtube_link = \DB::table('settings')->where('key','youtube_link')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','phone1')->count()){
            $phone1 = \DB::table('settings')->where('key','phone1')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','phone2')->count()){
            $phone2 = \DB::table('settings')->where('key','phone2')->select('value')->first()->value;
        }
        if(\DB::table('settings')->where('key','email')->count()){
            $email = \DB::table('settings')->where('key','email')->select('value')->first()->value;
        }

        return view('backend.pages.settings.index',
            compact('counter1_name','counter1_value','counter2_name',
                            'counter2_value','counter3_name','counter3_value',
                            'counter4_name','counter4_value', 'orange_number',
                            'vodafoon_number','bank_account','account_code',
                            'fb_link','insta_link','youtube_link','phone1',
                            'phone2','email'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //dd($request->all());
        $data = $request->validate([
            'counter1_name'=>'required|string',
            'counter1_value'=>'required|integer',
            'counter2_name'=>'required|string',
            'counter2_value'=>'required|integer',
            'counter3_name'=>'required|string',
            'counter3_value'=>'required|integer',
            'counter4_name'=>'required|string',
            'counter4_value'=>'required|integer',
            'orange_number'=>'required|string',
            'vodafoon_number'=>'required|string',
            'bank_account'=>'required|string',
            'account_code'=>'required|string',
            'fb_link'=>'required|url',
            'insta_link'=>'required|url',
            'youtube_link'=>'required|url',
            'phone1'=>'required|string',
            'phone2'=>'required|string',
            'email'=>'required|email',
        ]);
        $list = [
            0 =>[
                'key'=>'counter1_name',
                'value'=>$data['counter1_name']
            ],
            1=>[
                'key'=>'counter1_value',
                'value'=>$data['counter1_value']
            ],
            2 =>[
                'key'=>'counter2_name',
                'value'=>$data['counter2_name']
            ],
            3=>[
                'key'=>'counter2_value',
                'value'=>$data['counter2_value']
            ],
            4 =>[
                'key'=>'counter3_name',
                'value'=>$data['counter3_name']
            ],
            5=>[
                'key'=>'counter3_value',
                'value'=>$data['counter3_value']
            ],
            6=>[
                'key'=>'counter4_name',
                'value'=>$data['counter4_name']
            ],

            7=>[
                'key'=>'vodafoon_number',
                'value'=>$data['vodafoon_number']
            ],
            8=>[
                'key'=>'bank_account',
                'value'=>$data['bank_account']
            ],

            9=>[
                'key'=>'insta_link',
                'value'=>$data['insta_link']
            ],
            10=>[
                'key'=>'youtube_link',
                'value'=>$data['youtube_link']
            ],
            11=>[
                'key'=>'phone1',
                'value'=>$data['phone1']
            ],
            12=>[
                'key'=>'phone2',
                'value'=>$data['phone2']
            ],
            13=>[
                'key'=>'email',
                'value'=>$data['email']
            ],
            14=>[
                'key'=>'account_code',
                'value'=>$data['account_code']
            ],
            15=>[
                'key'=>'fb_link',
                'value'=>$data['fb_link']
            ],
            16=>[
                'key'=>'orange_number',
                'value'=>$data['orange_number']
            ],
            17=>[
                'key'=>'counter4_value',
                'value'=>$data['counter4_value']
            ],

        ];



       foreach ($list as $key => $item){
           if(Setting::where('key',$item['key'])->count()){
               $setting= Setting::where('key',$item['key'])->first();
               $setting->value = $item['value'];
               $setting->save();
           }else{
               $setting = new Setting();
               $setting->key = $item['key'];
               $setting->value = $item['value'];
               $setting->save();
           }

       }
        session()->flash('success','تم حفظ الخبر الجديد بنجاح، شكراً لك!');
        return  back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function show(Setting $setting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Setting $setting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Setting $setting)
    {
        //
    }
}
