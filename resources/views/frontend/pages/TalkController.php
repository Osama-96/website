<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreTalkRequest;
use App\Http\Requests\UpdateSlideRequest;
use App\Http\Requests\UpdateTalkRequest;
use App\Talk;
use foo\bar;
use Illuminate\Http\Request;

class TalkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Talk::orderByDesc('id')->paginate();
        return view('backend.pages.talks.index',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.pages.talks.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTalkRequest $request)
    {
        $data = $request->validated();
        $image = $this->uploadImage($request,'file','talks');

        $item = new Talk();
        $item->name = $data['name'];
        $item->job  = $data['job'];
        $item->text = $data['text'];
        $item->file = $image;
        $item->save();
        session()->flash('success','تم حفظ العنصر بنجاح..');
        return  redirect()->route('talks.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function show(Talk $talk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function edit(Talk $talk)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTalkRequest $request, Talk $talk)
    {
        $data = $request->validated();
        $item = $talk;
        if($request->hasFile('file')){
            \File::delete($item->file);
            $image = $this->uploadImage($request,'file','talks');
            $item->file = $image;
        }


        $item->name = $data['name'];
        $item->job  = $data['job'];
        $item->text = $data['text'];
        $item->save();
        session()->flash('success','تم تحديث بيانات العنصر العنصر بنجاح..');
        return  back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\talk  $talk
     * @return \Illuminate\Http\Response
     */
    public function destroy(Talk $talk)
    {
        \File::delete($talk->file);
        $talk->delete();
        session()->flash('success','تم  حذف العنصر بنجاح..');
        return back();
    }
}
