@extends('frontend.app')
@section('title') About Us @stop
@section('content')
<!-- WRAPPER-->
<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content">
                <div class="section  page-title set-height-top" dir="rtl">
                    <div class="container">
                        <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">من نحن</h2>
                            <ol class="breadcrumb">
                                <li><a href="{{route('/')}}">الرئيسية</a></li>
                                <li class="active"><a href="#">من نحن</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="section">
                    <div class="search-input">
                        <div class="container">
                            <div class="search-input-wrapper row ">
                                <div class="col-md-12 text-center ">
                                    <h2 style="color:#ffffff">جمعية مصر المحروسة بلدي</h2>
                                    <h5 style="color:#ffffff">(تنمية - تربية - تعليم)</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--INTRO EDUGATE-->
                <div class="section intro-edu" dir="rtl">
                    <div class="container">
                        <div class="intro-edu-wrapper">
                            <div class="row justify-content-center">
                                <div class="col-md-5 text-center">
                                    <img src="{{asset('assets/images/about1.png')}}" alt="" style="width: 60%; text-align: left"  class="intro-image fadeInLeft animated wow"/>
                                </div>
                                <div class="col-md-7">
                                    <div class="intro-title">من  <b style="color: #242c42">نحن</b> ؟</div>
                                    <div class="intro-content">
                                        <p>جمعية مصر المحروسة بلدي جمعية أهلية تنموية مصرية مركزية  تابعة لوزارة التضامن الإجتماعي تأسست عام 2000 لخدمة المجتمع عن طريق بناء الإنسان  .</p>
                                        <h4 style="color: #242c42">  رؤيتنا : </h4>
                                        <strong> منظمة تنموية تربوية تبنى الإنسان لتحقيق سعادته . </strong>

                                        <h4 style="color: #242c42; margin-top: 25px;">  رسالتنا : </h4>
                                        <p> تشارك الجمعية في جهود التنمية ببرامج تنموية وتعليمية للطفولة والشباب لتمكنهم من استثمار قدراتهم والموارد المتاحة، تدفعهم روح العطاء والانتماء تحقيقاً لسعادة الفرد والمجتمع.  </p>

                                        <p> كما تسعى لتحقيق النمو والانتشار بتقديم نماذج مؤسسية متطورة وبناء الثقة لدى أعضاء الجمعية العمومية والأفراد والجهات الداعمة، والتكامل مع المؤسسات التنموية، كما تهتم بالعاملين في جميع الجوانب.  </p>
                                    </div>

                                </div>
                                <div class="col-md-6" style="float: right">
                                    <div style="margin: 40px 0">
                                        <h4 style="color: #242c42">  القيم الحاكمة : </h4>
                                        <ul style="font-size: 16px;list-style-type: square;">
                                            <li style="margin:10px 0 ;"><strong>الشفافية والمسائلة: </strong> إتاحة المعلومات وحق المسائلة من الجهات المعنية والمستفيدين.</li>
                                            <li style="margin:10px 0 ;"><strong>المشاركة: </strong>مشاركة الجهات المعنية والمستفيدة في عمليات تخطيط وتنفيذ وتقييم المشروعات والأنشطة.</li>
                                            <li style="margin:10px 0 ;"><strong>المشاركة: </strong>مشاركة الجهات المعنية والمستفيدة في عمليات تخطيط وتنفيذ وتقييم المشروعات والأنشطة.</li>
                                            <li style="margin:10px 0 ;"><strong>الشورى والديموقراطية: </strong>تشجيع جو الحرية والمشاركة في اتخاذ القرار.</li>
                                            <li style="margin:10px 0 ;"><strong>المبادرة: </strong>تشجيع الإبداع والإبتكار.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-6" style="float: right">
                                    <div style="margin: 40px 0">
                                        <h4 style="color: #242c42">  سياسات الجمعية : </h4>
                                        <ul style="font-size: 16px ;list-style-type: square;" >
                                            <li style="margin:10px 0 ;">الانفتاح والتواصل مع الآخر، والتعاون مع الأفراد والهيئات.</li>
                                            <li style="margin:10px 0 ;">الشفافية وإتاحة المعلومات والبيانات المالية.</li>
                                            <li style="margin:10px 0 ;">العمل في الممكن والمتاح.</li>
                                            <li style="margin:10px 0 ;">التركيز على أوجه الأنشطة التربوية.</li>
                                            <li style="margin:10px 0 ;">الاهتمام بمشاركة المرأة والشباب، وتمثيلهم في المواقع المختلفة في الجمعية.</li>
                                            <li style="margin:10px 0 ;">التطوير المستمر والتدريب للموارد البشرية من العاملين بالجمعية.</li>
                                            <li style="margin:10px 0 ;">شمول التربية لكل الجوانب الإنسانية وجميع فئات المجتمع، خاصة الشباب والطفولة.</li>
                                            <li style="margin:10px 0 ;">التركيز على الجانب العملي والميداني.</li>
                                            <li style="margin:10px 0 ;">احترام التخصص.</li>
                                            <li style="margin:10px 0 ;">العمل بالمشاركة وتقديم المصلحة العامة على المصلحة الخاصة.</li>
                                            <li style="margin:10px 0 ;">عدم التمييز في أداء الخدمة على أساس الجنس أو الدين أو التوجه الفكري.</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12" style="float: right; text-align: center ">
                                    <img src="{{asset('assets/images/build.svg')}}" alt="" style="width: 60%; text-align: left" width="60%" class="intro-image fadeInLeft animated wow"/>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-10 " style="margin: 50px 0;" >
                                    <h4 style="color: #242c42;margin-bottom: 25px; ">   التاريخ والنشأة : </h4>
                                   <p style="font-size: 15px">اجتمعت إرادة عدد من المهتمين بأمر وطنهم على المساهمة الجادة والمستمرة في جهود التمية في المجتمع؛ فكانت الانطلاقة الأولى لجمعية مصر المحروسة عام 2000م داخل مقر صغير بحي المعادي بالقاهرة، وتم وضع رؤيتها ورسالتها وأهدافها العامة، وكانت الغاية التي تسعى إليها هي
                                       <strong style="color:  #31945c;">(بناء الإنسان)</strong>
                                       ، ولما كان بناء الإنسان له مداخل متعددة ومتنوعة؛ تبنت الجمعية المدخل القيمي والتربوي، ولذلك تجد المتفحص في برامج ومشروعات الجمعية يلمح أن المكون القيمي والتربوي مكون أساسي في التخطيط لجميع الأنشطة والفاعليات.
                                   </p>
                                    <p style="font-size: 15px">لم يأت هذا الأمر من فراغ، حيث إن معظم المؤسسين والعاملين في الجمعية م نالتربويين، فتجد المعلم والأخصائي الاجتماعي والأخصائي النفسي هم الذين يقومون على تنفيذ وتحقيق أهداف الجمعية من خلال فريق عمل متناغم ومتفاني يقوده أكثرهم وأوسعهم خبرة تربوية ضمن إطار مؤسسي منظم.</p>
                                    <p style="font-size: 15px">استمرت مسيرة العمل بالتكامل مع قطاعات الدولة ذات الصلة، والعديد من شركاء التنمية لتنفيذ المشروعات والمبادرات تحت مظلة 3 برامج رئيسية: </p>
                                    <ol style="font-size: 15px">
                                        <li style="margin:10px 0 ;">برنامج التعليم</li>
                                        <li style="margin:10px 0 ;">برنامج حماية الطفل</li>
                                        <li style="margin:10px 0 ;">برنامج التنمية المجتمعية</li>
                                    </ol>
                                    <p style="font-size: 15px"> و
                                        <?php echo \carbon\carbon::create(2000,1,1)->diffForHumans(); ?>
                                        هي عمر الجمعية نجحنا في الوصول إلى عدد كبير من المستفيدين في المجتمعات المهمشة داخل 12 محافظة، ومن خلال 21 مكتب تمثيل، وأكثر من 100 موظف وموظفة، وعدد كبير من المتطوعين. </p>
                                    <p style="font-size: 15px"> وبناءً على هذا التوسع والانتشار تحولت الجمعية إلى جمعية مركزية عام 2013م ومازالت مسيرة العطاء مستمرة ومتدفقة بفضل الله تعالى ثم بفضل جهود العاملين والمتطوعين حباً وتضحية لرفعة مصرنا الحبيب. </p>
                                    <p style="font-size: 15px"> وفي كل يوم تشرق فيه الشمس نتطلع فيه لمزيد من الإنجاز والعطاء تحقيقاً للغاية التي رسمناها منذ البداية وهي <strong style="color:  #31945c;">(بناء الإنسان)</strong> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section section-padding edu-ab">
                    <div class="container" dir="rtl">
                        <div class="edu-ab">
                            <div class="group-title-index edu-ab-title"><h2 class="center-title"> <b>برامج الجمعية</b> </h2><h4 class="top-title">تسعى الجمعية أن يكون لها نشاط تنموي وإصلاحي في المجتمع من خلال البرامج التالية</h4></div>
                            <div class="edu-ab-content">
                                <ul class="list-unstyled list-inline">
                                    <li>
                                        <a href="" style="color: #49575f" data-toggle="tooltip" title="اضغط لمعرفة التفاصيل">
                                            <div class="circle-icon"><i class="fa fa-child fa-2x"></i></div>
                                            <span> برنامج حماية الطفل (مشروع المنارات)</span>
                                        </a>
                                    </li>

                                    <li>
                                        <div class="circle-icon fa-2x"><i class="fa fa-signal"></i></div>
                                        <span>	برنامج التنمية الشاملة </span></li>
                                    <li>
                                        <div class="circle-icon fa-2x"><i class="fa fa-mortar-board"></i></div>
                                        <span>	برنامج التعليم</span></li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section section-padding edu-feature">
                    <div class="container" dir="rtl">
                        <div class="edu-feature-wrapper">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="group-title-index edu-feature-text"><h2 class="center-title">التنمية التي ننشدها</h2>

                                                <p class="top-title">
                                                    هدفنا كمؤسسة أن نساهم في جهود التنمية في مجتمعنا بداية من أنفسنا، واهتماماً بغرس القيم الفاضلة في النفوس لحماية المجتمع من التفكك. تنمية تستلزم الصدق والإخلاص، فهما روح كل عمل منفذ ومنجز، تنمية تدفع للتقدم والرقي، لا نتعجل فيها النتائج، تتحقق بأيدي الجمية، نؤيد فيها كل من يقدم خدمة للمجتمع، نسعى لإبراز مظاهرها في برامجنا ومشروعاتنا..
                                                </p>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <ul id="accordion" role="tablist" class="panel-group list-unstyled edu-feature-list">
                                                <li class="panel">
                                                    <div role="tab" id="item-1" class="col-item-1"><i class="fa fa-check-circle"></i><a href="#lesson" data-toggle="collapse" data-parent="#accordion" role="button" aria-expanded="true" aria-controls="lesson"><span>برنامج حماية الطفل</span> (مشروع المنارة)</a></div>
                                                    <div id="lesson" role="tabpanel" aria-labelledby="item-1" class="collapse in">
                                                        <div class="panel-body col-item-2">
                                                          قامت الجمعية بتصميم مشروع المنارة لتقديم كافة الخدمات الاجتماعية والتعليمية والثقافية والصحية والاقتصادية للنهوض بالطفل اليتيم وأسرته بكافة الجوانب، وذلك في عدد
                                                            <strong style="color:  #31945c;"> (17) منارة</strong>
                                                            ، وحقق المشروع نجاحاً باهراً، بعدد
                                                            <strong style="color:  #31945c;"> (2037) مستفيد</strong>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="panel">
                                                    <div role="tab" id="item-2" class="col-item-1"><i class="fa fa-check-circle"></i><a href="#display" data-toggle="collapse" data-parent="#accordion" role="button" aria-expanded="true" aria-controls="lesson"><span>برنامج التنمية الشاملة</span> ( مكون الإقراض)</a></div>
                                                    <div id="display" role="tabpanel" aria-labelledby="item-2" class="collapse">
                                                        <div class="panel-body col-item-2">
                                                            <strong style="color:  #31945c;">(مكون التمويل متناهي الصغر / مكون التمكين الاقتصادي)</strong>
                                                            يهدف هذا المكون لاتاحة التسهيلات المالية والتركيز على قوة العمل المهمشة من النساء..
                                                            يستهدف: المرأة وخاصة التي تعول أسرة بمفردها، صغار المزارعين من الأسر الفقيرة، أصحاب الحرف اليدوية، ذوي الإحتياجات الخاصة ممن لديهم الرغبة والقدرة على الإنتاج..
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="panel">
                                                    <div role="tab" id="item-3" class="col-item-1"><i class="fa fa-check-circle"></i><a href="#discuss" data-toggle="collapse" data-parent="#accordion" role="button" aria-expanded="true" aria-controls="lesson"><span>برنامج التنمية الشاملة</span> (مكون تحسين البيئة المعيشية)</a></div>
                                                    <div id="discuss" role="tabpanel" aria-labelledby="item-3" class="collapse">
                                                        <div class="panel-body col-item-2">
                                                            نسعى من خلاله إلى المساهمة في تحسين نوعية الحياة للفقراء؛ بتقديم كافة الخدمات التي توفر للإنسان حاجاته الأساسية، من خلال:
                                                            <ol>
                                                                <li style="border: none;">دعم توفير بيئة سكن آمنة.</li>
                                                                <li style="border: none;">دعم المرافق الأساسية في منزل المعيشة.</li>
                                                                <li style="border: none;">بناء أو تطوير أو ترميم منزل المعيشة.</li>
                                                            </ol>
                                                            نستهدف الأسر التي لا يزيد متوسط دخلها عن المعايير السنوية للفقر في جمهورية مصر العربية، وليس لديهم ممتلكات عقارية، أو من تهالكت البنية التحتية لمنازلهم منهم..
                                                        </div>
                                                    </div>
                                                </li>
                                                <li class="panel">
                                                    <div role="tab" id="item-4" class="col-item-1"><i class="fa fa-check-circle"></i><a href="#capability1" data-toggle="collapse" data-parent="#accordion" role="button" aria-expanded="true" aria-controls="lesson"><span>برنامج التعليم</span> </a></div>
                                                    <div id="capability1" role="tabpanel" aria-labelledby="item-4" class="collapse">
                                                        <div class="panel-body col-item-2">
                                                            ويتضمن هذا البرنامج
                                                            <strong style="color:  #31945c;">(برنامج رياض الأطفال بجمعية مصر المحروسة بلدي)</strong>
                                                            مجموعة متنوعة من الأنشطة الممتعة في مجالات الفنون والحرف اليدوية والألعاب، بحيث تكون هذه الأنشطة مصممة لتعليم الأطفال مهارات مختلفة..، والهدف منه هو الحد من ظاهرة التسرب من التعليم، وتوفير فرصة تعليمية ذات جودة عالية للأطفال في المناطق الأكثر احتياجاً.
                                                        </div>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6"><img src="{{asset('assets/images/charity.svg')}}" alt="" class="computer-image"/></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- PROGRESS BARS-->
            @include('frontend.includes.progress')



            <!-- BEST STAFF-->

                <!-- SLIDER TALK ABOUT US-->
                <div class="section background-opacity slider-talk-about-us">
                    <div class="container">
                        <div id="people-talk" data-ride="carousel" data-interval="5000" class="slider-talk-about-us-wrapper carousel slide">
                            <div role="listbox" class="slider-talk-about-us-content carousel-inner">
                                @foreach(\App\Talk::inRandomOrder()->get() as  $key=>$item)
                                    <div class="peopel-item item  {{($key==0)?'active':''}}">
                                        <p class="peopel-comment ">
                                            {{$item->text}}
                                        </p>

                                        <div class="group-peole-info">
                                            <div class="peopel-avatar"><img src="{{$item->file}}" alt="" class="img-responsive img-circle" style="width:150px; height: 100px;"/></div>
                                            <div class="peopel-name">{{$item->name}}</div>
                                            <div class="people-job">{{$item->job}}</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="group-btn-slider"><a href="#people-talk" role="button" data-slide="prev">
                            <div class="btn-prev carousel-control left"><i class="fa fa-angle-left"></i></div>
                        </a><a href="#people-talk" role="button" data-slide="next">
                            <div class="btn-next carousel-control right"><i class="fa fa-angle-right"></i></div>
                        </a></div>
                </div>
                <!-- SLIDER LOGO-->

            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->
@stop
