@extends('frontend.app')
@section('title') {{$item->title}} @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
        .galaryImage{
            position: relative;
            cursor: zoom-in;
        }
        .page-title1{
            background-image: url("{{asset($item->images[0])}}");
            background-repeat: repeat;
            background-attachment: fixed;
            background-position: top;
            width: 100%;
            height: 300px;
            color: #fff;
            text-align: center
        }
        .page-title1 .container {
            height: 100%;
            display: table
        }

        .page-title1 .page-title-wrapper {
            display: table-cell;
            vertical-align: middle;
            position: relative;
            z-index: 2
        }

        .page-title1 .captions {
            font-size: 42px;
            font-weight: 900;
            text-transform: uppercase;
            line-height: 1;
            margin: 0 0 7px 0
        }

        .page-title1 .breadcrumb {
            margin: 0;
            background-color: transparent;
            text-transform: capitalize
        }

        .page-title1 .breadcrumb li {
            display: inline-block
        }

        .page-title1 .breadcrumb li a {
            color: #cccccc
        }

        .page-title1 .breadcrumb li a:hover {
            color: #86bc42
        }

        .page-title1 .breadcrumb li:last-child {
            pointer-events: none
        }

        .page-title1 .breadcrumb li.active a {
            color: #fff
        }

        .page-title .breadcrumb li + li:before {
            padding: 0 10px;
            color: #cccccc;
            content: '\f105';
            font-family: FontAwesome
        }


        .col-xs-6{
            float: right;
        }

        /*slides*/


        /* Create four equal columns that floats next to eachother */
        .column {
            float: right;
            width: 25%;
        }


        /* The Modal (background) */
        .modal {
            display: none;
            position: fixed;
            z-index: 99999;
            padding-top: 100px;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            overflow: auto;
            background-color: black;
        }

        /* Modal Content */
        .modal-content {
            position: relative;
            background-color: #fefefe;
            margin: auto;
            padding: 0;
            width: 90%;
            max-width: 1200px;
        }

        /* The Close Button */
        .close {
            color: white;
            position: absolute;
            top: 10px;
            right: 25px;
            font-size: 35px;
            font-weight: bold;
        }

        .close:hover,
        .close:focus {
            color: #999;
            text-decoration: none;
            cursor: pointer;
        }

        /* Hide the slides by default */
        .mySlides {
            display: none;
        }

        /* Next & previous buttons */
        .prev,
        .next {
            cursor: pointer;
            position: absolute;
            top: 50%;
            width: auto;
            padding: 16px;
            margin-top: -50px;
            color: white;
            font-weight: bold;
            font-size: 20px;
            transition: 0.6s ease;
            border-radius: 0 3px 3px 0;
            user-select: none;
            -webkit-user-select: none;
        }

        /* Position the "next button" to the right */
        .next {
            left: 0;
            border-radius: 3px 0 0 3px;
        }

        /* On hover, add a black background color with a little bit see-through */
        .prev:hover,
        .next:hover {
            background-color: rgba(0, 0, 0, 0.8);
        }

        /* Number text (1/3 etc) */
        .numbertext {
            color: #f2f2f2;
            font-size: 12px;
            padding: 8px 12px;
            position: absolute;
            top: 0;
        }

        /* Caption text */
        .caption-container {
            text-align: center;
            background-color: black;
            padding: 2px 16px;
            color: white;
        }

        img.demo {
            opacity: 0.6;
            width: 100%;

        }

        .active,
        .demo:hover {
            opacity: 1;
        }

        img.hover-shadow {
            transition: 0.3s;
        }

        .hover-shadow:hover {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

    </style>
@endpush
@section('content')
    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content">
                    <div class="section  page-title1 background-opacity set-height-top"  dir="rtl">
                        <div class="container">
                            <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">{{$item->title}}</h2>
                                <ol class="breadcrumb">
                                    <li><a href="{{route('/')}}">الرئيسية</a></li>
                                    <li><a href="{{route('site_activities')}}">انشطة الجمعية</a></li>
                                    <li class="active"><a href="#">{{$item->title}}</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div class="search-input">
                            <div class="container">
                                <div class="search-input-wrapper row ">
                                    <div class="col-md-12 text-center ">
                                        <h2 style="color:#ffff">جمعية مصر المحروسة بلدي</h2>
                                        <h5 style="color:#ffff">(تنمية - تربية - تعليم)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section section-padding courses-detail event-detail-section">
                        <div class="container">
                            <div class="courses-detail-wrapper">
                                <div class="row" dir="rtl">
                                    <div class="col-md-9 layout-left">
                                        <h1 class="event-detail-title">{{$item->title}}</h1>

                                        <div class="course-info info info-event-detail">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="note-time-block"><span class="label-time">التاريخ:</span><span class="note-time">{{$item->created_at->toDayDateTimeString()}}</span></div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="event-detail-des">
                                            <div class="event-detail-des-content">
                                                {!! $item->content !!}
                                            </div>

                                        </div>
                                        @if($item->images[0])
                                        <div class="course-info info info-event-detail">
                                            <div class="row">
                                                <h4>الصور</h4>
                                                @foreach($item->images as $key => $image)
                                                <div class="col-md-4 col-sm-6 col-xs-6">
                                                    <div class="note-time-block">
                                                        <img src="{{asset($image)}}" onclick="openModal();currentSlide({{$key+1}})" alt="" style="width: 100%" class="img-responsive hover-shadow"/>
                                                    </div>
                                                </div>
                                                @endforeach
                                                <div id="myModal" class="modal">
                                                    <span class="close cursor" onclick="closeModal()">&times;</span>
                                                    <div class="modal-content">

                                                        @foreach($item->images as $key => $image)
                                                        <div class="mySlides">
                                                            <div class="numbertext">{{$key+1}} / {{count($item->images)}}</div>
                                                            <img src="{{asset($image)}}" style="width:100%">
                                                        </div>
                                                        @endforeach


                                                        <!-- Next/previous controls -->
                                                        <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                                                        <a class="next" onclick="plusSlides(1)">&#10095;</a>

                                                        <!-- Caption text -->
                                                        <div class="caption-container">
                                                            <p id="caption"></p>
                                                        </div>

                                                        <!-- Thumbnail image controls -->
                                                            @foreach($item->images as $key => $image)
                                                            <div class="column">
                                                                <img class="demo" style="max-height: 400px;" src="{{asset($image)}}" onclick="currentSlide({{$key+1}})" >
                                                            </div>
                                                            @endforeach
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                        @if($item->videos[0] )
                                        <div class="course-info info info-event-detail">
                                            <div class="row">
                                                <h4> الفديوهات:</h4>
                                                @foreach($item->videos as $key => $video)
                                                <div class="col-md-4">
                                                    <div class="note-time-block">
                                                        <div class="embed-responsive embed-responsive-16by9">
                                                            <iframe class="embed-responsive-item" style="height:100% " src=" https://www.youtube.com/embed/{{$video}}" allowfullscreen></iframe>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="col-md-3 sidebar layout-right">
                                        <div class="row">
                                            <div class="popular-course-widget widget col-sm-6 col-md-12 col-xs-6 sd380">
                                                <div class="title-widget">روابط ذات صلة</div>
                                                <div class="content-widget">

                                                    @foreach(\App\Activity::inRandomOrder()->limit(7)->get() as $key=>$row)
                                                    <div class="media">
                                                        <div class="media-left">
                                                            @if(!is_null($row->images))
                                                            <a href="{{route('site_activities_details',$row)}}" class="link">
                                                                <img src="{{asset($row->images[0])}}"  alt="" class="media-image"/>
                                                            </a>


                                                            @else
                                                                @if(!is_null($row->videos))
                                                                    <div class="embed-responsive embed-responsive-16by9">
                                                                        <iframe class="embed-responsive-item" style="height:100% " src=" https://www.youtube.com/embed/{{$row->videos[0]}}" allowfullscreen></iframe>
                                                                    </div>
                                                                @else
                                                                    <img src="{{asset('backend/images/noImage.png')}}" alt="" class="img-responsive img-rounded"/>
                                                                @endif
                                                            @endif
                                                        </div>
                                                        <div class="media-right">
                                                            <a href="{{route('site_activities_details',$row)}}" class="link title">{{$row->title}}</a>
                                                            <div class="info">
                                                                <div class="author item"><a href="#"><span>{{$row->created_at->toDayDateTimeString()}}</span></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach


                                                </div>
                                            </div>


                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>


                </div>
            </div>
        </div>

        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>
@stop
@section('js')
    <script !src="">
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var recipient = button.data('src')
            var modal = $(this)
            console.log(recipient)
            modal.find('.modal-body img').attr("src", recipient);
        })

        // Open the Modal
        function openModal() {
            document.getElementById("myModal").style.display = "block";
        }

        // Close the Modal
        function closeModal() {
            document.getElementById("myModal").style.display = "none";
        }

        var slideIndex = 1;
        showSlides(slideIndex);

        // Next/previous controls
        function plusSlides(n) {
            showSlides(slideIndex += n);
        }

        // Thumbnail image controls
        function currentSlide(n) {
            showSlides(slideIndex = n);
        }

        function showSlides(n) {
            var i;
            var slides = document.getElementsByClassName("mySlides");
            var dots = document.getElementsByClassName("demo");
            var captionText = document.getElementById("caption");
            if (n > slides.length) {slideIndex = 1}
            if (n < 1) {slideIndex = slides.length}
            for (i = 0; i < slides.length; i++) {
                slides[i].style.display = "none";
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" active", "");
            }
            slides[slideIndex-1].style.display = "block";
            dots[slideIndex-1].className += " active";
            captionText.innerHTML = dots[slideIndex-1].alt;
        }
    </script>
@stop
