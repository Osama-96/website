@extends('frontend.app')
@section('title') Contact Us @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
    </style>
@endpush
@section('content')
<!-- WRAPPER-->

<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content">
                <div class="section  page-title set-height-top" dir="rtl">
                    <div class="container">
                        <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">تواصل معنا</h2>
                            <ol class="breadcrumb">
                                <li><a href="{{route('/')}}">الرئيسية</a></li>
                                <li class="active"><a href="#">تواصل معنا</a></li>
                            </ol>
                            @if ($errors->any())
                                <div class="alert alert-danger text-center">لم يتم إرسال رسالتك برجاء التأكد من البيانات المكتوبة..</div>
                            @endif

                        </div>
                    </div>
                </div>
                <div class="section">
                    <div class="search-input">
                        <div class="container">
                            <div class="search-input-wrapper row ">
                                <div class="col-md-12 text-center ">
                                    <h2 style="color:#ffffff">جمعية مصر المحروسة بلدي</h2>
                                    <h5 style="color:#ffffff">(تنمية - تربية - تعليم)</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="section section-padding contact-main">
                    <div class="container" dir="rtl">
                        <div class="contact-main-wrapper">
                            <div class="row contact-method " >
                                <div class="col-md-6 right" >
                                    <div class="method-item"><i class="fa fa-map-marker"></i>

                                        <p class="sub">المقر الرئيسي</p>

                                        <div class="detail"><p style="font-size: 14px;">أرض الخدمات بالمعصرة  - بجوار مجمع المدارس – طريق الأوتوستراد  </p>

                                            <p>- القاهرة -</p></div>
                                    </div>
                                </div>
                                <div class="col-md-3"  style="min-height: 250px;">
                                    <div class="method-item"><i class="fa fa-phone"></i>

                                        <p class="sub">أرقام الهاتف</p>

                                        <div class="detail"><p>{{\DB::table('settings')->where('key','phone1')->select('value')->first()->value}}  - {{\DB::table('settings')->where('key','phone2')->select('value')->first()->value}}</p>

                                            <p></p></div>
                                    </div>
                                </div>
                                <div class="col-md-3" style="min-height: 250px;">
                                    <div class="method-item"><i class="fa fa-envelope"></i>

                                        <p class="sub">الإيميل</p>

                                        <div class="detail"><p>{{\DB::table('settings')->where('key','email')->select('value')->first()->value}}</p>

                                            </div>
                                    </div>
                                </div>
                            </div>
                            <form class="bg-w-form contact-form" action="{{route('mails.store')}}" method="POST">
                                @csrf
                                <div class="row" dir="rtl">
                                    <div class="col-md-6" >
                                        <div class="form-group">
                                            <label class="control-label form-label">الاسم <span class="highlight">*</span></label>
                                            @error('name')
                                            <small class="text-danger">{{$message}}</small>
                                            @enderror

                                            <input type="text" maxlength="30" placeholder="" class="form-control form-input"name="name" value="{{old('name')}}" required/><!--label.control-label.form-label.warning-label(for="") Warning for the above !--></div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label class="control-label form-label">الهاتف <span class="highlight">*</span></label>
                                            @error('phone')
                                            <small class="text-danger">{{$message}}</small>
                                            @enderror
                                            <input type="text" maxlength="11" placeholder="" class="form-control form-input"name="phone" value="{{old('phone')}}" required/><!--label.control-label.form-label.warning-label(for="")--></div>

                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label form-label">الغرض</label>
                                            @error('reason')
                                            <small class="text-danger">{{$message}}</small>
                                            @enderror
                                            <select class="form-control form-input selectbox" name="reason"  required>
                                                <option value="التواصل من أجل التبرع" {{(old('reason') == 'التواصل من أجل التبرع')?'selected':''}}>التواصل من أجل التبرع</option>
                                                <option value="التواصل من أجل طلب الدعم" {{(old('reason') == 'التواصل من أجل طلب الدعم')?'selected':''}}>التواصل من أجل طلب الدعم</option>
                                                <option value="التواصل من أجل الاستفسار" {{(old('reason') == 'التواصل من أجل الاستفسار')?'selected':''}}>التواصل من أجل الاستفسار</option>
                                                <option value="غرض آخر" {{(old('reason') == 'غرض آخر')?'selected':''}} >غرض آخر</option>
                                            </select><!--label.control-label.form-label.warning-label(for="", hidden)-->

                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="contact-question form-group">
                                            <label class="control-label form-label">من فضلك قم بترك رسالتك.. <span class="highlight">*</span></label>
                                            @error('message')
                                            <small class="text-danger">{{$message}}</small>
                                            @enderror
                                            <textarea name="message" maxlength="700" class="form-control form-input" required>{{old('message')}}</textarea></div>

                                    </div>
                                </div>
                                <div class="contact-submit">
                                    <button type="submit" class="btn btn-contact btn-green"><span>إرسال</span></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1625.6661988618266!2d31.301900858067466!3d29.908051795483036!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x1458379c1892c295%3A0xc3d5539a72270082!2z2KzZhdi52YrYqSDZhdi12LEg2KfZhNmF2K3YsdmI2LPYqSDYqNmE2K_Zig!5e1!3m2!1sen!2sus!4v1594982858001!5m2!1sen!2sus" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>                <div id="map" >
                </div>
            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>

<!-- FOOTER-->
@stop
@section('js')
    <script src="{{asset('assets/js/pages/contact.js')}}"></script>
@stop
