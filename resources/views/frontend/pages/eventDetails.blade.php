@extends('frontend.app')
@section('title') Contact Us @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
        .galaryImage{
            position: relative;
            cursor: zoom-in;
        }

    </style>
@endpush
@section('content')
    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content">
                    <div class="section  page-title set-height-top" dir="rtl">
                        <div class="container">
                            <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">حفلة عيد الأم</h2>
                                <ol class="breadcrumb">
                                    <li><a href="{{route('/')}}">الرئيسية</a></li>
                                    <li><a href="{{route('galary')}}">مكتبة الصور</a></li>
                                    <li class="active"><a href="#">حفلة عيد الأم</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div class="search-input">
                            <div class="container">
                                <div class="search-input-wrapper row ">
                                    <div class="col-md-12 text-center ">
                                        <h2 style="color:#ffff">جمعية مصر المحروسة بلدي</h2>
                                        <h5 style="color:#ffff">(تنمية - تربية - تعليم)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section section-padding courses-detail event-detail-section">
                        <div class="container">
                            <div class="courses-detail-wrapper">
                                <div class="row" dir="rtl">
                                    <div class="col-md-12 ">
                                        <h1 class="event-detail-title">{{$item->title}}</h1>

                                        <div class="course-info info info-event-detail">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="note-time-block"><span class="label-time">التاريخ:</span><span class="note-time">{{$item->created_at->toDayDateTimeString()}}</span></div>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="address-info"><span class="label-time">المكان</span>جامعة القاهرة - الجيزة </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="event-detail-thumb"><img src="assets/images/event-detail-1.jpg" alt="" class="img-responsive"/></div>
                                        <div class="event-detail-des">
                                            <div class="event-detail-des-content"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed varius ultricies metus. Donec ac ex porta libero venenatis sodales. Sed efficitur eget risus sed molestie. Nulla blandit bibendum metus ut sagittis. Etiam quis semper justo. Sed tristique facilisis felis ut tincidunt. Phasellus auctor convallis nisl ut accumsan. Suspendisse ullamcorper fermentum lectus, vel tincidunt ligula mollis sit amet. Aliquam at ante at elit efficitur tincidunt a quis neque. Donec ut pulvinar metus. Pellentesque lobortis volutpat eros sed sagittis.</p><br/>

                                                <p>Nunc rutrum ex eu auctor tristique. Maecenas suscipit vestibulum nunc nec placerat. Phasellus blandit augue nunc, consequat consectetur augue placerat sed. Aenean fermentum scelerisque lectus, sit amet ultricies ex interdum bibendum. Quisque porttitor, enim maximus convallis gravida, dui arcu lacinia libero, quis ornare nibh elit pharetra massa.</p></div>
                                            <div class="course-syllabus-title underline">الصور</div>
                                            <div class="row" >
                                                <div class="col-md-4" style="margin:25px 0 ;">
                                                    <img class="img-thumbnail " data-toggle="modal" data-target="#exampleModal"  style="width: 100%; cursor: zoom-in" data-src="assets/images/lib-pictures/pictures-2.jpg"  src="{{asset('assets/images/lib-pictures/pictures-2.jpg')}}" alt="">
                                                </div>
                                                <div class="col-md-4" style="margin:25px 0 ;">
                                                    <img class="img-thumbnail " data-toggle="modal" data-target="#exampleModal"  style="width: 100%; cursor: zoom-in" data-src="{{asset('assets/images/event-detail-1.jpg')}}"  src="{{asset('assets/images/lib-pictures/pictures-2.jpg')}}" alt="">
                                                </div>
                                                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                    <div class="modal-dialog modal-xl">
                                                        <div class="modal-content">
                                                            <div class="modal-header " style="border-bottom: none">
                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                </button>
                                                            </div>
                                                            <div class="modal-body">
                                                                <img src="" style="width: 100%;" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>
@stop
@section('js')
    <script !src="">
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var recipient = button.data('src')
            var modal = $(this)
            console.log(recipient)
            modal.find('.modal-body img').attr("src", recipient);
        })
    </script>
@stop
