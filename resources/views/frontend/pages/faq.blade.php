@extends('frontend.app')
@section('title') F&Q @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
    </style>
@endpush
@section('content')
<!-- WRAPPER-->

<div id="wrapper-content"><!-- PAGE WRAPPER-->
    <div id="page-wrapper"><!-- MAIN CONTENT-->
        <div class="main-content"><!-- CONTENT-->
            <div class="content">
                <div class="section  page-title set-height-top" dir="rtl">
                    <div class="container">
                        <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">الأسئلة الشائعة</h2>
                            <ol class="breadcrumb">
                                <li><a href="{{route('/')}}">الرئيسية</a></li>
                                <li class="active"><a href="#">الأسئلة الشائعة</a></li>
                            </ol>
                        </div>
                    </div>
                </div>
                <div class="section">
                    <div class="search-input">
                        <div class="container">
                            <div class="search-input-wrapper row ">
                                <div class="col-md-12 text-center ">
                                    <h2 style="color:#ffffff">جمعية مصر المحروسة بلدي</h2>
                                    <h5 style="color:#ffffff">(تنمية - تربية - تعليم)</h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- FREQUENTLY ASKED QUESTIONS-->
                <!-- FREQUENTLY ASKED QUESTIONS-->
                <div class="section section-padding">
                    <div class="container" dir="rtl">
                        <div class="group-title-index"><h4 class="top-title">هنا تجد الإجابة على ما يرد إلينا من</h4>

                            <h2 class="center-title">الأسئلة الشائعة</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-icon-05"></i></div>
                        </div>
                        <div class="accordion-faq">
                            <div class="row justify-content-center" style="">

                                <div class="col-md-12">
                                    <div class="underline">استفسارات المتابعين</div>
                                    <div id="accordion-2" class="panel-group accordion">

                                        @foreach($data as $key => $row)
                                            <div class="panel {{($key==0)?'active':''}}">
                                                <div class="panel-heading">
                                                    <h5 class="panel-title"><a data-toggle="collapse" data-parent="#accordion-2" href="#collapse-2-{{$key}}" aria-expanded="{{($key==0)?'true':'false'}}" class="accordion-toggle {{($key==0)?'':'collapsed'}}">{{$row->title}}</a></h5></div>
                                                <div id="collapse-2-{{$key}}" style="" aria-expanded="{{($key==0)?'true':'false'}}" class="panel-collapse collapse {{($key==0)?'in':''}}">
                                                    <div class="panel-body">
                                                        {!! $row->content !!}
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- CONTACT-->

            </div>
        </div>
    </div>
    <!-- BUTTON BACK TO TOP-->
    <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
</div>
<!-- FOOTER-->
@stop
@section('js')
    <script src="assets/js/pages/faq.js"></script>
@stop
