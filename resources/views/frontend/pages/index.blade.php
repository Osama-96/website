@extends('frontend.app')
@section('title') Home @stop
@section('css')
    <style>
        @media (min-width: 768px){
            .carousel-inner > .item > img, .carousel-inner > .item {
                height: 550px;
            }
        }
        @media (max-width: 768px){
            .carousel-inner > .item > img, .carousel-inner > .item {
                height: 400px;
            }
        }

    </style>
@stop

@section('content')
    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content"><!-- SLIDER BANNER-->
                    <div class="section ">
                        <div id="myCarousel" class="carousel slide " data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                @foreach(\App\Slider::all() as $key => $item)
                                <li data-target="#myCarousel" data-slide-to="{{$key}}" class="{{($key==0)?'active':''}}"></li>
                                @endforeach
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                @foreach(\App\Slider::all() as $key => $item)
                                <div class="item {{($key==0)?'active':''}}" >
                                    <img src="{{asset($item->file)}}" style="width: 100%; height: 100%;" alt="" >
                                    @if($item->text || $item->link)
                                    <div class="carousel-caption d-none d-md-block text-black-60 text-center"style="color: #1b1e21; background-color: rgba(255,255,255,0.2) ">
                                        <p style="font-size: 18px;" >{{$item->text}}</p>
                                        <a href="{{$item->link}}" class="btn btn-green" style="background-color: #333333eb;margin-bottom: 25px;"><span>اتبع الرابط</span></a>
                                    </div>
                                    @endif
                                </div>
                                @endforeach

                            </div>

                            <!-- Left and right controls -->

                            <a class="left carousel-control " href="#myCarousel" data-slide="prev"></a>
                            <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
                        </div>
                    </div>
                    <!-- TOP COURSES-->
                    <div class="section section-padding top-courses" >
                        <div class="container">
                            <div class="group-title-index mt-5">

                                <h2 class="center-title ">أخبار الجمعية </h2>

                                <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                            </div>
                            <div class="top-courses-wrapper">
                                <div class="top-courses-slider">
                                    <?php
                                    $newsGroup1_id = \App\News::orderByDesc('id')->limit(5)->pluck('id')->toArray();
                                    $newsGroup1 = \App\News::whereIn('id',$newsGroup1_id)->select(['slug','title','created_at','visit','content','image'])->get();
                                    if(\App\News::count() > 10){
                                    $newsGroup2_id = \App\News::whereNotIn('id',$newsGroup1_id)->inRandomOrder()->limit(5)->pluck('id');
                                    $newsGroup2 = \App\News::whereIn('id',$newsGroup2_id)->select(['slug','title','created_at','visit','content','image'])->get();
                                    }

                                    ?>
                                    @foreach( $newsGroup1 as $key => $row)

                                        <div class="top-courses-item" >
                                            <div class="edugate-layout-2">
                                                <div class="edugate-layout-2-wrapper" style="text-align: right; direction: rtl">
                                                    <div class="edugate-content" style="text-align: right">
                                                        <a href="{{route('Site_News_Show',$row->slug)}}" class="title" >{{$row->title}}</a>

                                                        <div class="info" style="text-align: right">

                                                            <div class="date-time item"><a href="#">{{$row->created_at->diffForHumans()??''}}</a></div>
                                                        </div>
                                                        <div class="info-more" style="text-align: right">
                                                            <div class="view item">
                                                                <i class="fa fa-eye"></i>

                                                                <p> {{$row->visit}}</p></div>
    {{--                                                        <div class="comment item"><i class="fa fa-comment"></i>--}}

    {{--                                                            <p> 239</p></div>--}}
                                                        </div>
    {{--                                                    <div title="Rated 5.00 out of 5" class="star-rating"><span class="width-80"><strong class="rating">5.00</strong> out of 5</span></div>--}}
                                                        <div class="description" style="text-align: right">{{substrwords(strip_tags($row->content),200)}}</div>
                                                        <button onclick="window.location.href='{{route('Site_News_Show',$row->slug)}}'" class="btn btn-green"><span>قراءة الخبر</span></button>
                                                    </div>
                                                    <div class="edugate-image"><img src="{{asset($row->image)}}" alt="" class="img-responsive img-rounded"/></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                                @if(\App\News::count() > 10)
                                <div class="top-courses-slider">

                                    @foreach( $newsGroup2 as $key => $row)
                                        <div class="top-courses-item" >
                                            <div class="edugate-layout-2">
                                                <div class="edugate-layout-2-wrapper" style="text-align: right; direction: rtl">
                                                    <div class="edugate-content" style="text-align: right"><a href="{{route('Site_News_Show',$row->slug)}}" class="title" >{{$row->title}}</a>

                                                        <div class="info" style="text-align: right">

                                                            <div class="date-time item"><a href="#">{{$row->created_at->diffForHumans()??''}}</a></div>
                                                        </div>
                                                        <div class="info-more" style="text-align: right">
                                                            <div class="view item"><i class="fa fa-user"></i>

                                                                <p> {{$row->visit}}</p></div>
                                                            {{--                                                        <div class="comment item"><i class="fa fa-comment"></i>--}}

                                                            {{--                                                            <p> 239</p></div>--}}
                                                        </div>
                                                        {{--                                                    <div title="Rated 5.00 out of 5" class="star-rating"><span class="width-80"><strong class="rating">5.00</strong> out of 5</span></div>--}}
                                                        <div class="description" style="text-align: right">{{substrwords(strip_tags($row->content),200)}}</div>
                                                        <button onclick="window.location.href='{{route('Site_News_Show',$row->slug)}}'" class="btn btn-green"><span>قراءة الخبر</span></button>
                                                    </div>
                                                    <div class="edugate-image"><img src="{{asset($row->image)}}" alt="" class="img-responsive img-rounded"/></div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                @endif
                                <div class="group-btn-top-courses-slider">
                                    <div class="btn-prev">&lsaquo;</div>
                                    <div class="btn-next">&rsaquo;</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- CHOOSE COURSES-->
                    <div class="section section-padding choose-course">
                        <div class="container">

                            <div class="group-title-index ">
                                <h2 class="center-title " >
                                    جمعية مصر المحروسة بلدي
                                </h2>

                                <h4 class="top-title" style="direction: rtl">هدفنا أن نساهم فى جهود الإصلاح فى مجتمعنا، والإصلاح يبدأ من أنفسنا.</h4>

                                <div class="bottom-title"><i class="bottom-icon icon-a-1-01-01"></i></div>
                            </div>
                            <div class="choose-course-wrapper row justify-content-center" dir="rtl">
                                <div class="col-md-12 col-xs-12">
                                    <video  controls  poster="{{asset('assets/images/video.png')}}" id="myVideo" style="width: 100%">
                                        <source src="{{asset('assets/videos/1.mp4')}}" type="video/mp4">
                                    </video>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- PROGRESS BARS-->
                    @include('frontend.includes.progress')


                    <!-- PRICING-->
                    <div class="section section-padding pricing">
                        <div class="container">
                            <div class="group-title-index">
{{--                                <h4 class="top-title">chooses your pricing</h4>--}}

                                <h2 class="center-title">رسائل تربوية</h2>

                                <div class="bottom-title"><i class="bottom-icon icon-a-1-01-01"></i></div>
                            </div>
                            <div id="people-talk-1" data-ride="carousel" data-interval="5000" class="slider-talk-about-us-wrapper carousel slide">
                                <div role="listbox" class="slider-talk-about-us-content carousel-inner text-center " >

                                    @foreach(\DB::table('edu_mesgs')->whereNull('deleted_at')->inRandomOrder()->get() as $key=> $edu)
                                    <div class="peopel-item item {{($key == 0)?'active':''}}" dir="rtl" style="max-height: 200px;">
                                        <p class="peopel-comment blockquote" dir="rtl">
                                            {{$edu->text}}
                                        </p>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- SLIDER TALK ABOUT US-->

                    <div class="section background-opacity slider-talk-about-us">
                        <div class="container">
                            <div class="group-title-index  " style="margin-top: 60px; color: white">

                                <h2 class="center-title" >قالوا عنا </h2>

                                <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                            </div>
                            <div id="people-talk" data-ride="carousel" data-interval="5000" class="slider-talk-about-us-wrapper carousel slide">
                                <div role="listbox" class="slider-talk-about-us-content carousel-inner ">
                                    @foreach(\App\Talk::inRandomOrder()->get() as  $key=>$item)
                                    <div class="peopel-item item  {{($key==0)?'active':''}}">
                                        <p class="peopel-comment ">
                                            {{$item->text}}
                                        </p>

                                        <div class="group-peole-info">
                                            <div class="peopel-avatar"><img src="{{$item->file}}" alt="" class="img-responsive img-circle" style="width:150px; height: 100px;"/></div>
                                            <div class="peopel-name">{{$item->name}}</div>
                                            <div class="people-job">{{$item->job}}</div>
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="group-btn-slider"><a href="#people-talk" role="button" data-slide="prev">
                                <div class="btn-prev carousel-control left"><i class="fa fa-angle-left"></i></div>
                            </a><a href="#people-talk" role="button" data-slide="next">
                                <div class="btn-next carousel-control right"><i class="fa fa-angle-right"></i></div>
                            </a>
                        </div>
                    </div>



                </div>
            </div>
        </div>
        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>
@stop
@push('js')
    <script !src="">
        $('#people-talk-1').hover(function() {
            $(this).carousel('pause');
        }, function() {
            $(this).carousel('cycle');
        });
    </script>
@endpush
