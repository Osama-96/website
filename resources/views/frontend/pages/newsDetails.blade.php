@extends('frontend.app')
@section('title') {{$item->title}} @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
        .galaryImage{
            position: relative;
            cursor: zoom-in;
        }
        .page-title1{
            background-image: url("{{asset($item->image)}}");
            background-repeat: repeat;
            background-attachment: fixed;
            background-position: top;
            width: 100%;
            height: 300px;
            color: #fff;
            text-align: center
        }
        .page-title1 .container {
            height: 100%;
            display: table
        }

        .page-title1 .page-title-wrapper {
            display: table-cell;
            vertical-align: middle;
            position: relative;
            z-index: 2
        }

        .page-title1 .captions {
            font-size: 42px;
            font-weight: 900;
            text-transform: uppercase;
            line-height: 1;
            margin: 0 0 7px 0
        }

        .page-title1 .breadcrumb {
            margin: 0;
            background-color: transparent;
            text-transform: capitalize
        }

        .page-title1 .breadcrumb li {
            display: inline-block
        }

        .page-title1 .breadcrumb li a {
            color: #cccccc
        }

        .page-title1 .breadcrumb li a:hover {
            color: #86bc42
        }

        .page-title1 .breadcrumb li:last-child {
            pointer-events: none
        }

        .page-title .breadcrumb li.active a {
            color: #fff
        }

        .page-title .breadcrumb li + li:before {
            padding: 0 10px;
            color: #cccccc;
            content: '\f105';
            font-family: FontAwesome
        }

    </style>
@endpush
@section('content')
    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content">
                    <div class="section  page-title1 background-opacity set-height-top"  dir="rtl">
                        <div class="container">
                            <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">{{$item->title}}</h2>
                                <ol class="breadcrumb">
                                    <li><a href="{{route('/')}}">الرئيسية</a></li>
                                    <li><a href="{{route('Site_News_Page')}}">أخبار الجمعية</a></li>
                                    <li class="active"><a href="#">{{$item->title}}</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div class="search-input">
                            <div class="container">
                                <div class="search-input-wrapper row ">
                                    <div class="col-md-12 text-center ">
                                        <h2 style="color:#ffff">جمعية مصر المحروسة بلدي</h2>
                                        <h5 style="color:#ffff">(تنمية - تربية - تعليم)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section section-padding courses-detail event-detail-section">
                        <div class="container">
                            <div class="courses-detail-wrapper">
                                <div class="row" dir="rtl">
                                    <div class="col-md-9 layout-left">
                                        <h1 class="event-detail-title">{{$item->title}}</h1>

                                        <div class="course-info info info-event-detail">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <div class="note-time-block"><span class="label-time">التاريخ:</span><span class="note-time">{{$item->created_at->toDayDateTimeString()}}</span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="event-detail-thumb"><img src="{{asset($item->image)}}" alt="" style="width: 100%" class="img-responsive"/></div>
                                        <div class="event-detail-des">
                                            <div class="event-detail-des-content">
                                                {!! $item->content !!}
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-3 sidebar layout-right">
                                        <div class="row">
                                            <div class="popular-course-widget widget col-sm-6 col-md-12 col-xs-6 sd380">
                                                <div class="title-widget">روابط ذات صلة</div>
                                                <div class="content-widget">

                                                    @foreach(\App\News::inRandomOrder()->limit(7)->get() as $key=>$news)
                                                    <div class="media">
                                                        <div class="media-left"><a href="{{route('Site_News_Show',$news)}}
                                                                " class="link">
                                                                <img src="{{asset($news->image)}}" alt="" class="media-image"/></a></div>
                                                        <div class="media-right">
                                                            <a href="{{route('Site_News_Show',$news)}}" class="link title">{{$news->title}}</a>
                                                            <div class="info">
                                                                <div class="author item"><a href="#"><span>{{$news->created_at->toDayDateTimeString()}}</span></a></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach


                                                </div>
                                            </div>


                                        </div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>
@stop
@section('js')
    <script !src="">
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var recipient = button.data('src')
            var modal = $(this)
            console.log(recipient)
            modal.find('.modal-body img').attr("src", recipient);
        })
    </script>
@stop
