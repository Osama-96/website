@extends('frontend.app')
@section('title') Our News @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
        .galaryImage{
            position: relative;
            cursor: zoom-in;
        }


    </style>
@endpush
@section('content')
    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content">
                    <div class="section  page-title  set-height-top"  dir="rtl">
                        <div class="container">
                            <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">أخبار الجمعية</h2>
                                <ol class="breadcrumb">
                                    <li><a href="{{route('/')}}">الرئيسية</a></li>
                                    <li class="active"><a href="#">أخبار الجمعية</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div class="search-input">
                            <div class="container">
                                <div class="search-input-wrapper row ">
                                    <div class="col-md-12 text-center ">
                                        <h2 style="color:#ffff">جمعية مصر المحروسة بلدي</h2>
                                        <h5 style="color:#ffff">(تنمية - تربية - تعليم)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section section-padding news-page">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="row news-page-wrapper">
                                        @foreach($data as $key=>$row)
                                        <div class="col-sm-4">
                                            <div class="edugate-layout-3 news-gird">
                                                <div class="edugate-layout-3-wrapper"><a href="{{route('Site_News_Show',$row)}}" class="edugate-image">
                                                        <img src="{{$row->image}}" alt="صورة الخبر" class="img-responsive"/></a>

                                                    <div class="edugate-content"><a href="{{route('Site_News_Show',$row)}}" class="title" style="height: 250px; overflow: hidden">{{$row->title}}</a>

                                                        <div class="info">
                                                            <div class="date-time item"><a href="#">{{$row->created_at->diffForHumans()??''}}</a></div>
                                                        </div>
                                                        <div class="info-more">
                                                            <div class="view item"><i class="fa fa-eye"></i>

                                                                <p> {{$row->visit}}</p>
                                                            </div>

                                                        </div>
                                                        <div class="description" style="height: 250px; overflow: hidden"> {{substrwords(strip_tags($row->content),150)}} </div>
                                                        <button class="btn btn-green" onclick="window.location.href='{{route('Site_News_Show',$row)}}'"><span>قراءة</span></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach

                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center justify-content-center" style="text-align: center" >
                                            {{$data->onEachSide(3)->links()}}
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>
@stop
@section('js')
    <script !src="">
        $('#exampleModal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var recipient = button.data('src')
            var modal = $(this)
            console.log(recipient)
            modal.find('.modal-body img').attr("src", recipient);
        })
    </script>
@stop
