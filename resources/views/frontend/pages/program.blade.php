@extends('frontend.app')
@section('title') Programs @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
        .galaryImage{
            position: relative;
            cursor: zoom-in;
        }
        .btn-next , .btn-prev{
            width: 40px;
            height: 40px;
            line-height: 36px;
            text-align: center;
            color: #fff;
            background-color: #31945c;
            display: inline-block;
            font-size: 24px;
            font-family: Helvetica, Arial, sans-serif;
            cursor: pointer;
            transition: all 0.2s linear;
        }
        .btn-prev{
            margin-left: 10px;
        }
        .edugate-layout-2:after {
            position: absolute;
            z-index: 2;
            content: '';
            width: 5px;
            top: 0;
            right: 0;
            height: 0;
            background-color: #31945c;
            transition: all 0.5s ease;
        }

    </style>



@endpush
@section('content')
    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content">
                    <div class="section  page-title set-height-top" dir="rtl">
                        <div class="container">
                            <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">{{$item->title}}</h2>
                                <ol class="breadcrumb">
                                    <li><a href="{{route('/')}}">الرئيسية</a></li>
                                    <li class="active"><a href="#">{{$item->title}}</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div class="search-input">
                            <div class="container">
                                <div class="search-input-wrapper row ">
                                    <div class="col-md-12 text-center ">
                                        <h2 style="color:#ffff">جمعية مصر المحروسة بلدي</h2>
                                        <h5 style="color:#ffff">(تنمية - تربية - تعليم)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section section-padding courses-detail event-detail-section" style="margin-bottom: 50px">
                        <div class="container" dir="rtl">
                            <div class="group-title-index">
                                <h2 class="center-title "  >فكرة البرنامج</h2>
                                <div class="bottom-title"><i class="bottom-icon icon-icon-05"></i></div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 text-center">
                                    <p>
                                        {{$item->content}}
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section section-padding  top-courses" style="background-color: #2b3551">
                        <div class="container">
                            <div class="group-title-index mt-5">
                                <h2 class="center-title "style="color: white"> أنشطة البرنامج </h2>
                                <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                            </div>

                            <div class="top-courses-wrapper">
                                <div class="top-courses-slider">
                                    @if($item->activities()->count() )
                                    @foreach($item->activities as $key => $row)
                                    <div class="top-courses-item" >
                                        <div class="edugate-layout-2">
                                            <div class="edugate-layout-2-wrapper" style="text-align: right; direction: rtl">
                                                <div class="edugate-content" style="text-align: right">
                                                    <a href="{{route('site_activities_details',$row)}}" class="title" >{{$row->title}}</a>

                                                    <div class="info" style="text-align: right">

                                                        <div class="date-time item"><a href="#">{{$row->created_at->diffForHumans()??''}}</a></div>
                                                    </div>
                                                    <div class="info-more" style="text-align: right">
                                                        <div class="view item">
                                                            <i class="fa fa-eye"></i>

                                                            <p> {{$row->visit}}</p></div>
                                                    </div>
                                                    {{--                                                    <div title="Rated 5.00 out of 5" class="star-rating"><span class="width-80"><strong class="rating">5.00</strong> out of 5</span></div>--}}
                                                    <div class="description" style="text-align: right">{{substrwords(strip_tags($row->content),200)}}</div>
                                                    <button onclick="window.location.href='{{route('site_activities_details',$row)}}'" class="btn btn-green"><span>الاطلاع على المحتوى </span></button>
                                                </div>
                                                <div class="edugate-image"><img src="{{asset($row->images[0])}}" alt="" class="img-responsive img-rounded"/></div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach
                                    @endif
                                </div>

                                <div class="group-btn-top-courses-slider">
                                    <div class="btn-prev">&lsaquo;</div>
                                    <div class="btn-next">&rsaquo;</div>
                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="section section-padding courses-detail event-detail-section">
                        <div class="container" dir="rtl">
                            <div class="group-title-index">
                                <h2 class="center-title">قصص نجاح</h2>
                                <h4 class="top-title">{{$item->story_sub_title}}</h4>
                                <div class="bottom-title"><i class="bottom-icon icon-icon-05"></i></div>
                            </div>
                            <div class="intro-edu-wrapper">
                                <div class="row">
                                    <div class="col-md-5 text-center">
                                        <img src="{{asset('assets/images/success.svg')}}" alt="" style="width: 60%; text-align: left" width="60%" class="intro-image fadeInRight animated wow"/>
                                        <p style=" padding: 25px;">
                                            {{$item->story_text}}
                                        </p>
                                    </div>

                                    <div class="col-md-7 justify-content-center">
                                        @if($item->stories()->count() > 0)
                                        <div id="myCarousel" class="carousel slide justify-content-center" style="margin-bottom:40px; " data-ride="carousel">

                                            <div class="carousel-inner">
                                                @foreach($item->stories as $key => $story)
                                                <div class="item {{($key == 0)?'active':''}} edugate-layout-3 news-gird">
                                                    <div class="edugate-layout-3-wrapper">
                                                        <div class="" style="width: 100%; text-align: center; padding-top: 50px; ">
                                                            <img class=" frame" style="width: 100%" src="https://img.youtube.com/vi/{{$story->link}}/hqdefault.jpg"  data-video="https://www.youtube.com/embed/{{$story->link}}" width="100%"  placeholder="" alt="">
                                                        </div>
                                                        <div class="edugate-content text-center">
                                                            <h2 class="title text-center" >{{$story->title}}</h2>
                                                            <div class="description text-center" style="overflow: unset;">
                                                                <p>
                                                                    {{$story->content}}
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                                <div class="group-btn-top-courses-slider" style=" margin-top: -25px; text-align: center;">
                                                    <a  class="btn-prev cycle" href="#myCarousel" role="button" data-slide="next">&lsaquo; </a>
                                                    <a class="btn-next cycle" href="#myCarousel" role="button" data-slide="prev">&rsaquo; </a>
                                                </div>
                                            </div>

                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section background-opacity slider-talk-about-us" ">
                        <div class="container">
                            <div class="group-title-index  " style="margin-top: 60px; color: white">

                                <h2 class="center-title" >تقدر تساعد </h2>
                                <h4 class="top-title">{{$item->can_help_sub_title}}</h4>

                                <div class="bottom-title"><i class="bottom-icon icon-icon-04"></i></div>
                            </div>
                            @if ($item->helps()->count())

                            <div id="people-talk" data-ride="carousel" data-interval="5000" class="slider-talk-about-us-wrapper carousel slide" style="padding-top:0px; ">
                                <div role="listbox" class="slider-talk-about-us-content carousel-inner ">
                                    @foreach($item->helps as  $key => $row)
                                    <div class="peopel-item item {{($key==0)?'active':''}} ">
                                        @if(!is_null($row->text ))
                                        <p class="peopel-comment ">{{$row->text}}</p>
                                        @endif
                                        @if (!is_null($row->file) )
                                                <div class="group-peole-info">
                                                    <div class=""><img style="display: inline-block;  max-height:200px " src="{{asset($row->file)}}" alt="" style="max-width: 400px;" class="img-responsive"/></div>

                                                </div>
                                        @endif

                                    </div>
                                    @endforeach

                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="group-btn-slider"><a href="#people-talk" role="button" data-slide="prev">
                                <div class="btn-prev carousel-control left"><i class="fa fa-angle-left"></i></div>
                            </a><a href="#people-talk" role="button" data-slide="next">
                                <div class="btn-next carousel-control right"><i class="fa fa-angle-right"></i></div>
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>
@stop
@section('js')
    <script >
        $('body').on('click','.frame',function() {
            video = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item youtube" style="height:100% " src="' + $(this).attr('data-video') + '" allowfullscreen></div>';
            $(this).replaceWith(video);
            $('.carousel').carousel('pause');
        });


    </script>
@stop
