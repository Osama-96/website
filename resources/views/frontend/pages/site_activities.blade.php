@extends('frontend.app')
@section('title') أنشطة الجمعية @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
        .galaryImage{
            position: relative;
            cursor: zoom-in;
        }

    </style>
@endpush
@section('content')

    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content">
                    <div class="section  page-title set-height-top" dir="rtl">
                        <div class="container">
                            <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">مكتبة الأنشطة</h2>
                                <ol class="breadcrumb">
                                    <li><a href="{{route('/')}}">الرئيسية</a></li>
                                    <li class="active"><a href="#">أنشطة الجمعية</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div class="search-input">
                            <div class="container">
                                <div class="search-input-wrapper row ">
                                    <div class="col-md-12 text-center ">
                                        <h2 style="color:#ffffff">جمعية مصر المحروسة بلدي</h2>
                                        <h5 style="color:#ffffff">(تنمية - تربية - تعليم)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section section-padding courses">
                        <div class="container">
                            <div class="group-title-index">
                                <h2 class="center-title">أرشيف الأنشطة</h2>
                                <h4 class="top-title">اطلع على كل الفديوهات والصور المتعلقة بأنشطة الجمعية</h4>
                                <div class="bottom-title"><i class="bottom-icon icon-icon-05"></i></div>
                            </div>
                            <div class="courses-wrapper"><!-- Nav tabs-->
                                <div class="row top-content">
                                    <div class="col-md-3">
                                        <div class="result-output text-left"><p class="result-count">عرض <strong>1-{{count($data)}}</strong> من مجموع <strong>{{\App\Activity::count()}}</strong> عنصر</p></div>
                                    </div>

                                </div>
                                <!-- Tab panes-->
                                <div class="tab-content courses-content">
                                    <div id="campus" role="tabpanel" class="tab-pane fade in active">
                                        <div class="style-show style-grid row">
                                            @foreach($data as $key => $row)
                                            <div class="col-style">
                                                <div class="edugate-layout-2">
                                                        <div class="edugate-layout-2-wrapper" style="text-align: right; direction: rtl">
                                                            <div class="edugate-content" style="text-align: right"><a href="{{route('site_activities_details',$row)}}" class="title" >{{$row->title}}</a>

                                                                <div class="info" style="text-align: right">

                                                                    <div class="date-time item"><a href="#">{{$row->created_at->toDayDateTimeString()??''}}</a></div>
                                                                </div>
                                                                <div class="info-more" style="text-align: right">
                                                                    <div class="view item"><i class="fa fa-eye"></i>

                                                                        <p> {{$row->visit}}</p></div>
                                                                    {{--                                                        <div class="comment item"><i class="fa fa-comment"></i>--}}

                                                                    {{--                                                            <p> 239</p></div>--}}
                                                                </div>
                                                                {{--                                                    <div title="Rated 5.00 out of 5" class="star-rating"><span class="width-80"><strong class="rating">5.00</strong> out of 5</span></div>--}}
                                                                <div class="description" style="text-align: right">{{substrwords(strip_tags($row->content),200)}}</div>
                                                                <button onclick="window.location.href='{{route('site_activities_details',$row)}}'" class="btn btn-green"><span>مشاهدة المحتوى</span></button>
                                                            </div>
                                                            <div class="edugate-image">
                                                                @if(!is_null($row->images))
                                                                <img src="{{asset($row->images[0])}}" alt="" class="img-responsive img-rounded"/>
                                                                @else
                                                                    @if(!is_null($row->videos))
                                                                        <div class="embed-responsive embed-responsive-16by9">
                                                                            <iframe class="embed-responsive-item" style="height:100% " src=" https://www.youtube.com/embed/{{$row->videos[0]}}" allowfullscreen></iframe>
                                                                        </div>
                                                                    @else
                                                                    <img src="{{asset('backend/images/noImage.png')}}" alt="" class="img-responsive img-rounded"/>
                                                                    @endif
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>

@stop
@section('js')

@stop
