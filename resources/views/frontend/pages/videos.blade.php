@extends('frontend.app')
@section('title') Contact Us @stop
@push('css')
    <style>
        @media (min-width: 992px){
            .col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
                float: right;
            }
        }
    </style>
@endpush
@section('content')
    <div id="wrapper-content"><!-- PAGE WRAPPER-->
        <div id="page-wrapper"><!-- MAIN CONTENT-->
            <div class="main-content"><!-- CONTENT-->
                <div class="content">
                    <div class="section  page-title set-height-top" dir="rtl">
                        <div class="container">
                            <div class="page-title-wrapper"><!--.page-title-content--><h2 class="captions">مكتبة الفديوهات</h2>
                                <ol class="breadcrumb">
                                    <li><a href="{{route('/')}}">الرئيسية</a></li>
                                    <li class="active"><a href="#">مكتبة الفديوهات</a></li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="section">
                        <div class="search-input">
                            <div class="container">
                                <div class="search-input-wrapper row ">
                                    <div class="col-md-12 text-center ">
                                        <h2 style="color:#ffffff">جمعية مصر المحروسة بلدي</h2>
                                        <h5 style="color:#ffffff">(تنمية - تربية - تعليم)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="section section-padding picture-gallery">
                        <div class="group-title-index"><h4 class="top-title">اطلع على كل جديد في</h4>

                            <h2 class="center-title">مكتبة الفديوهات</h2>

                            <div class="bottom-title"><i class="bottom-icon icon-a-1-01"></i></div>
                        </div>
                        <div class="picture-gallery-wrapper"><!-- Nav tabs-->
                            <!-- Tab panes-->

                            <div class=" container">
                                <div class="edugate-layout-1 " style="margin: 50px 0;" dir="rtl" >
                                    <div class="edugate-image">
                                        <img class=" frame" style="width: 100%" src="https://img.youtube.com/vi/PufSiDaEz8c/hqdefault.jpg"  data-video="https://www.youtube.com/embed/PufSiDaEz8c" width="100%" height="200" placeholder="" alt="">

                                    </div>
                                    <div class="edugate-content"><a href="news-detail.html" class="title">عنوان الفيديو</a>

                                        <div class="info">
                                            <div class="author item"><a href="#">By Admin</a></div>
                                            <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                        </div>
                                        <div class="info-more">
                                            <div class="view item"><i class="fa fa-user"></i>

                                                <p> 56</p></div>
                                            <div class="comment item"><i class="fa fa-comment"></i>

                                                <p> 239</p></div>
                                        </div>
                                        <div class="description">
                                            لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق "ليتراسيت" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل "ألدوس بايج مايكر" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.
                                        </div>
                                    </div>
                                </div>
                                <div class="edugate-layout-1 " style="margin: 50px 0;" >
                                    <div class="edugate-image">
                                        <img class=" frame" style="width: 100%" src="https://img.youtube.com/vi/cbBqzATb0VQ/hqdefault.jpg"  data-video="https://www.youtube.com/embed/cbBqzATb0VQ" width="100%" height="200" placeholder="" alt="">

                                    </div>
                                    <div class="edugate-content"><a href="news-detail.html" class="title">عنوان الفيديو</a>

                                        <div class="info">
                                            <div class="author item"><a href="#">By Admin</a></div>
                                            <div class="date-time item"><a href="#">17 sep 2015</a></div>
                                        </div>
                                        <div class="info-more">
                                            <div class="view item"><i class="fa fa-user"></i>

                                                <p> 56</p></div>
                                            <div class="comment item"><i class="fa fa-comment"></i>

                                                <p> 239</p></div>
                                        </div>
                                        <div class="description">
                                            لوريم إيبسوم(Lorem Ipsum) هو ببساطة نص شكلي (بمعنى أن الغاية هي الشكل وليس المحتوى) ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد الإلكتروني. انتشر بشكل كبير في ستينيّات هذا القرن مع إصدار رقائق "ليتراسيت" (Letraset) البلاستيكية تحوي مقاطع من هذا النص، وعاد لينتشر مرة أخرى مؤخراَ مع ظهور برامج النشر الإلكتروني مثل "ألدوس بايج مايكر" (Aldus PageMaker) والتي حوت أيضاً على نسخ من نص لوريم إيبسوم.
                                        </div>
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- BUTTON BACK TO TOP-->
        <div id="back-top"><a href="#top"><i class="fa fa-angle-double-up"></i></a></div>
    </div>


@stop
@push('js')

    <script >
        $('body').on('click','.frame',function() {
            video = '<div class="embed-responsive embed-responsive-16by9"><iframe class="embed-responsive-item" style="height:100% " src="' + $(this).attr('data-video') + '" allowfullscreen></div>';
            $(this).replaceWith(video);
        });
    </script>
@endpush
