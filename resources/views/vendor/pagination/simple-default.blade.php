@if ($paginator->hasPages())
    <nav>
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled" aria-disabled="true"><span>السابق</span></li>
            @else
                <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev">السابق</a></li>
            @endif

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li><a href="{{ $paginator->nextPageUrl() }}" rel="next">التالي</a></li>
            @else
                <li class="disabled" aria-disabled="true"><span>التالي</span></li>
            @endif
        </ul>
    </nav>
@endif
