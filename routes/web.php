<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/', 'frontend.pages.index')->name('/');
Route::view('about-us', 'frontend.pages.about')->name('about');
Route::view('contact-us', 'frontend.pages.contact')->name('contact');
Route::get('F&Q', 'getPagesCtrl@faq')->name('faq');
Route::get('site_activities', 'getPagesCtrl@site_activities')->name('site_activities');
Route::get('site_activities/{id:slug}', 'getPagesCtrl@site_activities_details')->name('site_activities_details');
Route::post('programs/{id:slug}/set_story', 'SuccessStoryController@store')->name('stories.store');
Route::post('programs/{id:slug}/set_help', 'NeedHelpController@store')->name('helps.store');
Route::view('eventDetails', 'frontend.pages.eventDetails')->name('eventDetails');
Route::view('videos', 'frontend.pages.videos')->name('videos');
Route::get('program/{id:slug}', 'getPagesCtrl@program')->name('program');
Route::view('new-news', 'backend.pages.news.new')->name('newNews');



Route::get('/home', 'HomeController@index')->name('home');
Route::get('our_news/{id:slug}','newsCtrl@show')->name('News_Show');
Route::get('site_news/{id:slug}','newsCtrl@showForUser')->name('Site_News_Show');
Route::get('site_news','newsCtrl@SiteIndex')->name('Site_News_Page');

Route::resource('news', 'newsCtrl')->except([
    'show',
]);
Route::resource('activities', 'ActivityController');
Route::resource('faqs', 'FaqController');
Route::resource('programs', 'ProgramController');
Route::resource('stories', 'SuccessStoryController')->except(['store','show','edit','index']);
Route::resource('helps', 'NeedHelpController')->except(['store','show','edit','index']);
Route::resource('slides', 'SliderController')->except(['show','edit']);
Route::resource('talks', 'TalkController')->except(['show','edit']);
Route::resource('mails', 'ContactUsController')->except(['show','edit']);
Route::resource('edumesgs', 'EduMesgController');
Route::resource('settings', 'SettingController');



Route::group(['middleware' => ['auth']], function()
{

    Route::get('settings', 'SettingController@index')->name('settings');
    Route::view('admin', 'backend.pages.index')->name('admin');

});

    Auth::routes(['register' => false,'reset'=>false]);

